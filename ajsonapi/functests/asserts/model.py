# Copyright © 2019-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts/model provides functions to test a simple instantiation of
the functional test model.
"""

from ajsonapi.functests.model_objects import (
    JSON_IDENTIFIER_MANY_ONES_UUID_31,
    JSON_IDENTIFIER_ONE_ONE_LOCALS_UUID_11,
    JSON_IDENTIFIER_ONE_ONE_REMOTES_UUID_21,
    JSON_IDENTIFIERS_MANY_MANYS_UUID_51_52,
    JSON_IDENTIFIERS_ONE_MANYS_UUID_41_42,
    UUID_1,
    UUID_2,
    UUID_3,
)


def assert_centers_uuid_1_unrelated(center):
    """Verifies that `center` is a /centers/{UUID_1} object without
    relationships.
    """

    assert center['type'] == 'centers'
    assert center['id'] == UUID_1
    assert 'attributes' in center
    attributes = center['attributes']
    assert 'attr_int' in attributes
    assert attributes['attr_int'] == 1
    assert 'attr_str' in attributes
    assert attributes['attr_str'] == 'one'
    assert 'relationships' in center
    relationships = center['relationships']
    assert relationships['one_one_local']['data'] is None
    assert relationships['one_one_remote']['data'] is None
    assert relationships['many_one']['data'] is None
    assert relationships['one_manys']['data'] == []
    assert relationships['many_manys']['data'] == []


def same_list(list0, list1):
    """Tests whether list0 and list1 contain the same elements.
    Note may not work correctly in case of duplicates!"""
    # Would love to use sets but not elements are hashable.
    return len(list0) == len(list1) and all(elem in list0 for elem in list1)


def assert_centers_uuid_1_related(center):
    """Verifies that `center` is a /centers/{UUID_1} object with
    relationships.
    """

    assert center['type'] == 'centers'
    assert center['id'] == UUID_1
    assert 'attributes' in center
    attributes = center['attributes']
    assert 'attr_int' in attributes
    assert attributes['attr_int'] == 1
    assert 'attr_str' in attributes
    assert attributes['attr_str'] == 'one'
    assert 'relationships' in center
    relationships = center['relationships']
    assert relationships['one_one_local']['data'] == \
        JSON_IDENTIFIER_ONE_ONE_LOCALS_UUID_11['data']
    assert relationships['one_one_remote']['data'] == \
        JSON_IDENTIFIER_ONE_ONE_REMOTES_UUID_21['data']
    assert relationships['many_one']['data'] == \
        JSON_IDENTIFIER_MANY_ONES_UUID_31['data']
    assert same_list(relationships['one_manys']['data'],
                     JSON_IDENTIFIERS_ONE_MANYS_UUID_41_42['data'])
    assert same_list(relationships['many_manys']['data'],
                     JSON_IDENTIFIERS_MANY_MANYS_UUID_51_52['data'])


def assert_centers_id_unrelated(center, id_=None):
    """Verifies that `center` is a /centers/{id_} object without relationships.
    """

    assert center['type'] == 'centers'
    if id_ is not None:
        assert center['id'] == id_
    assert 'attributes' in center
    attributes = center['attributes']
    assert 'attr_int' in attributes
    assert attributes['attr_int'] == 1
    assert 'attr_str' in attributes
    assert attributes['attr_str'] == 'one'
    assert 'relationships' in center
    relationships = center['relationships']
    assert relationships['one_one_local']['data'] is None
    assert relationships['one_one_remote']['data'] is None
    assert relationships['many_one']['data'] is None
    assert relationships['one_manys']['data'] == []
    assert relationships['many_manys']['data'] == []


def assert_centers_id_related(center, id_=None):
    """Verifies that `center` is a /centers/{id_} object with relationships."""

    assert center['type'] == 'centers'
    if id_ is not None:
        assert center['id'] == id_
    assert 'attributes' in center
    attributes = center['attributes']
    assert 'attr_int' in attributes
    assert attributes['attr_int'] == 1
    assert 'attr_str' in attributes
    assert attributes['attr_str'] == 'one'
    assert 'relationships' in center
    relationships = center['relationships']
    assert relationships['one_one_local']['data'] == \
        JSON_IDENTIFIER_ONE_ONE_LOCALS_UUID_11['data']
    assert relationships['one_one_remote']['data'] == \
        JSON_IDENTIFIER_ONE_ONE_REMOTES_UUID_21['data']
    assert relationships['many_one']['data'] == \
        JSON_IDENTIFIER_MANY_ONES_UUID_31['data']
    assert same_list(relationships['one_manys']['data'],
                     JSON_IDENTIFIERS_ONE_MANYS_UUID_41_42['data'])
    assert same_list(relationships['many_manys']['data'],
                     JSON_IDENTIFIERS_MANY_MANYS_UUID_51_52['data'])


def assert_centers_uuid_2_unrelated(center):
    """Verifies that `center` is a /centers/{UUID_2} object without
    relationships.
    """

    assert center['type'] == 'centers'
    assert center['id'] == UUID_2
    assert 'attributes' in center
    attributes = center['attributes']
    assert 'attr_int' in attributes
    assert attributes['attr_int'] == 2
    assert 'attr_str' in attributes
    assert attributes['attr_str'] == 'two'
    assert 'relationships' in center
    relationships = center['relationships']
    assert relationships['one_one_local']['data'] is None
    assert relationships['one_one_remote']['data'] is None
    assert relationships['many_one']['data'] is None
    assert relationships['one_manys']['data'] == []
    assert relationships['many_manys']['data'] == []


def assert_centers_uuid_3_unrelated(center):
    """Verifies that `center` is a /centers/{UUID_3} object without
    relationships.
    """

    assert center['type'] == 'centers'
    assert center['id'] == UUID_3
    assert 'attributes' in center
    attributes = center['attributes']
    assert 'attr_int' in attributes
    assert attributes['attr_int'] == 3
    assert 'attr_str' in attributes
    assert attributes['attr_str'] == 'three'
    assert 'relationships' in center
    relationships = center['relationships']
    assert relationships['one_one_local']['data'] is None
    assert relationships['one_one_remote']['data'] is None
    assert relationships['many_one']['data'] is None
    assert relationships['one_manys']['data'] == []
    assert relationships['many_manys']['data'] == []
