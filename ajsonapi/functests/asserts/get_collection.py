# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts.get_collection provides functions to test the GET
/{collection} responses from the JSON API application.
"""

from ajsonapi.functests.asserts.generic import (
    assert_data_object,
    assert_nonexistent,
)


#
# Successful requests/responses
#
async def assert_get_collection(response):
    """Verifies a response to a successful GET /{collection} request."""

    assert response.status == 200
    assert response.headers['Content-Type'] == 'application/vnd.api+json'
    document = await response.json()
    assert isinstance(document, dict)
    assert 'data' in document
    assert 'links' in document

    data = document['data']
    assert isinstance(data, list)
    for obj in data:
        assert_data_object(obj)

    links = document['links']
    assert isinstance(links, dict)
    assert 'self' in links
    url = response.url
    assert links['self'] in [url, url.path]

    return document


#
# Failed requests/responses
#
async def assert_get_collection_nonexistent_collection(response, path):
    """Verifies the response to a failed GET /{collection} request where
    {collection} does not exist.
    """

    return await assert_nonexistent(response, path)
