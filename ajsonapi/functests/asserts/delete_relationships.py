# Copyright © 2019-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts.post_relationships provides functions to test the DELETE
/{collection}/{id}/relationships/{relationship} responses from the JSON API
application.
"""

from ajsonapi.functests.asserts.generic import assert_errors, assert_no_content


#
# Successful requests/responses
#
async def assert_delete_relationship(response):
    """Verifies the response to a successful DELETE
    /{collection}/{id}/relationships/{relationship} request.
    """

    await assert_no_content(response)


#
# Failed requests/responses
#
async def assert_delete_to_one_relationship(response):
    """Verifies the response to a failed DELETE
    /{collection}/{id}/relationships/{relationship} request where the
    {relationship} is a to-one relationship.
    """

    assert response.status == 405
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '405'
    assert error['title'] == 'Method not allowed.'
    return document
