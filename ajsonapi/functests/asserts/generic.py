# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts.generic provides assert functions that help test the
responses from the JSON API application.
"""

VALID_DATA_OBJECT_KEYS = {'type', 'id', 'attributes', 'relationships'}


#
# Successful requests/responses
#
def assert_data_object(obj):
    """Verifies the correctness of a return document's data object.

    Args:
        obj (dict): JSON representation of a response document's resource
            object.

    Raises:
        AssertError: The obj does not represent a JSON API data object.
    """

    assert isinstance(obj, dict)
    assert set(obj.keys()) <= VALID_DATA_OBJECT_KEYS
    assert 'type' in obj
    assert 'id' in obj
    assert isinstance(obj['type'], str)
    assert isinstance(obj['id'], str)


async def assert_object(response):
    """Verifies the response to a successful GET or PATCH /{collection}/{id}
    request.
    """
    assert response.status == 200
    assert response.headers['Content-Type'] == 'application/vnd.api+json'
    document = await response.json()
    assert isinstance(document, dict)
    assert 'data' in document
    assert 'links' in document

    obj = document['data']
    assert_data_object(obj)

    links = document['links']
    assert isinstance(links, dict)
    assert 'self' in links
    url = response.url
    assert links['self'] in [url, url.path]

    return document


async def assert_no_content(response):
    """Verifies a 204 No Content response."""

    assert response.status == 204
    assert 'Content-Type' not in response.headers
    text = await response.text()
    assert text == ''


#
# Failed requests/responses
#
async def assert_errors(response):
    """Verifies the correctness of an error response.

    Args:
        response: Response to a failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors response.
    """

    assert response.headers['Content-Type'] == 'application/vnd.api+json'
    document = await response.json()
    assert isinstance(document, dict)
    assert 'errors' in document

    errors = document['errors']
    assert isinstance(errors, list)
    for error in errors:
        assert 'status' in error
        assert 'title' in error

    return document


async def assert_nonexistent(response, path, detail=None):
    """Verifies the response to a failed request with a url that does not exist.

    Args:
        response: Response to the failed request.
        path: Expected path of the response document's error source.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response with a nonexistent url.
    """

    assert response.status == 404
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '404'
    assert error['title'] == 'Resource does not exist.'
    if detail:
        assert error['detail'] == detail
    else:
        assert 'detail' not in error
    assert error['source']['path'] == path
    return document


async def assert_data(response, title, detail, pointers):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains an error.

    Args:
        response: Response to the failed request.
        title (str): Expected title of the response error.
        detail (str): Expected detail of the response error.
        pointers (set(str)): Expected pointers of the response errors' source.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data error.
    """

    assert response.status == 422
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == len(pointers)
    error_pointers = {error['source']['pointer'] for error in errors}
    assert error_pointers == pointers
    for error in errors:
        assert error['status'] == '422'
        assert error['title'] == title
        assert error['detail'] == detail
    return document


async def assert_data_missing(response):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member is
    missing.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's missing data.
    """

    return await assert_data(
        response,
        title='Invalid document.',
        detail="Missing 'data' member at document's top level.",
        pointers={''})  # Note: this is the correct pointer value (not a dummy).


async def assert_data_malformed(response):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member is not
    a resource object.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's malformed data.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail="Malformed 'data' member.",
                             pointers={'/data'})


async def assert_data_missing_type(response, pointer='/data'):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object without a type.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data missing a type.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail='Missing type.',
                             pointers={pointer})


async def assert_data_missing_id(response, pointer='/data'):
    """Verifies a response to a failed PATCH /{collection}/{id} or PATCH
    /{collection}/{id}/relationships/{relationship} request where the request
    document's data member contains a resource object without an id.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data missing an id.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail='Missing id.',
                             pointers={pointer})


async def assert_data_invalid(response, detail, pointers):
    """Verifies the response to a failed POST /{collection} request where the
    request document's data member contains a resource object with an invalid
    type or id.
    """

    assert response.status == 409
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == len(pointers)
    error_pointers = {error['source']['pointer'] for error in errors}
    assert error_pointers == pointers
    for error in errors:
        assert error['status'] == '409'
        assert error['title'] == 'Invalid resource object.'
        assert error['detail'] == detail
    return document


async def assert_data_invalid_type(response, pointer):
    """Verifies the response to a failed POST /{collection} request where the
    request document's data member contains a resource object with an invalid
    type.
    """

    return await assert_data_invalid(response,
                                     detail='Invalid type.',
                                     pointers={pointer})


async def assert_data_malformed_id(response, pointer):
    """Verifies the response to a failed PATCH/POST
    /{collection}/{id}/relationships/{relationship} request where the request
    document's body contains a resource identifier object with a malformed
    id.
    """
    assert response.status == 409
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '409'
    assert error['title'] == 'Invalid resource identifier object.'
    assert error['detail'] == 'Malformed id.'
    assert error['source']['pointer'] == pointer
    return document


async def assert_data_nonexistent_id(response, pointers):
    """Verifies the response to a failed PATCH/POST
    /{collection}/{id}/relationships/{relationship} request where the request
    document's body contains a resource identifier object with a nonexistent
    id.
    """
    assert response.status == 422
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == len(pointers)
    error_pointers = {error['source']['pointer'] for error in errors}
    assert error_pointers == pointers
    for error in errors:
        assert error['status'] == '422'
        assert error['title'] == 'Invalid resource identifier object.'
        assert error['detail'] == 'Nonexistent id.'
    return document


async def assert_data_invalid_attribute(response, pointer):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object with an invalid attribute.

    Args:
        response: Response to the failed request.
        pointer (str): Expected pointer of the response error's source.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data containing an invalid
            attribute.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail='Invalid attribute.',
                             pointers={pointer})


async def assert_data_invalid_relationship(response, pointer):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object with an invalid relationship.

    Args:
        response: Response to the failed request.
        pointer (str): Expected pointer of the response error's source.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data containing an invalid
            relationship.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail='Invalid relationship.',
                             pointers={pointer})


async def assert_data_malformed_relationship(response, pointers):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object with a malformed relationship.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail='Malformed relationship.',
                             pointers=pointers)


async def assert_data_malformed_relationship_data(response, pointers):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object with a malformed relationship data member.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail='Malformed relationship data.',
                             pointers=pointers)


async def assert_data_missing_relationship_type(response, pointers):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object with a relationship without a type.

    Args:
        response: Response to the failed request.
        pointers (set(str)): Expected pointers of the response errors' sources.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data containing a relationship
            without a type.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail='Missing relationship type.',
                             pointers=pointers)


async def assert_data_invalid_relationship_type(response, pointers):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object with a relationship with an invalid type.

    Args:
        response: Response to the failed request.
        pointers (set(str)): Expected pointers of the response errors' sources.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data containing a relationship
            with an invalid type.
    """

    return await assert_data_invalid(response,
                                     detail='Invalid relationship type.',
                                     pointers=pointers)


async def assert_data_missing_relationship_id(response, pointers):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object with a relationship without an id.

    Args:
        response: Response to the failed request.
        pointers (set(str)): Expected pointers of the response errors' sources.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data containing a relationship
            without an id.
    """

    return await assert_data(response,
                             title='Invalid resource object.',
                             detail='Missing relationship id.',
                             pointers=pointers)


async def assert_data_malformed_relationship_id(response, pointers):
    """Verifies the response to a failed POST /{collection} or PATCH
    /{collection}/{id} request where the request document's data member
    contains a resource object with a relationship with a malformed id.

    Args:
        response: Response to the failed request.
        pointers (set(str)): Expected pointers of the response errors' sources.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request document's data containing a relationship
            with a malformed id.
    """

    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == len(pointers)
    error_pointers = {error['source']['pointer'] for error in errors}
    assert error_pointers == pointers
    for error in errors:
        assert error['status'] == '409'
        assert error['title'] == 'Invalid resource identifier object.'
        assert error['detail'] == 'Malformed relationship id.'
    return document


async def assert_document_not_json(response):
    """Verifies the response to a failed request because the request document
    is not JSON (or missing).

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request containing a document that is not JSON.
    """

    assert response.status == 400
    document = await assert_errors(response)

    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '400'
    assert error['title'] == 'Invalid document.'
    assert error['detail'] == 'Document is not JSON.'
    assert 'source' not in error
    return document


async def assert_content_type_parameter(response):
    """Verifies the response to a request with a Content-Type header
    containing a JSON API media type ('application/vnd.api+json') with a media
    type parameter.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with a Content-Type header containing a JSON
            API media type with a media type parameter.
    """

    assert response.status == 415
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '415'
    assert error['title'] == 'Invalid header.'
    assert error['source']['header'] == 'Content-Type'
    return document


async def assert_accept_parameters(response):
    """Verifies the response to a request with an Accept header containing a
    JSON API media type ('application/vnd.api+json') without a media type
    parameter.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with an Accept header containing a JSON
            API media type without a media type parameter.
    """

    assert response.status == 406
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '406'
    assert error['title'] == 'Invalid header.'
    assert error['source']['header'] == 'Accept'
    return document


async def assert_include_invalid_path(response, path):
    """Verifies the response to a request that includes an 'include' query
    parameter with an invalid path.
    """
    assert response.status == 400
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error == {
        'status': '400',
        'source': {
            'parameter': 'include'
        },
        'title': 'Invalid query parameter.',
        'detail': f"The resource does not have a '{path}' relationship path."
    }
    return document


async def assert_unsupported_query(response, parameter):
    """Verifies the response to a request that includes an unsupported query
    parameter.

    Args:
        response: Response to the failed request.
        parameter (str): Expected parameter of the response error's source.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with an unsupported query parameter.
    """

    assert response.status == 400
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '400'
    assert error['title'] == 'Parameter not supported.'
    assert error['source']['parameter'] == parameter
    return document


async def assert_duplicate_parameters(response, parameter):
    """Verifies the response to a request that includes duplicate parameters
    with the same key (for "include" and "sort").

    Args:
        response: Response to the failed request.
        parameter (str): Expected parameter of the response error's source.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with an unsupported query parameter.
    """

    assert response.status == 400
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '400'
    assert error['title'] == 'More than one parameter with the same key.'
    assert error['source']['parameter'] == parameter
    return document


async def assert_query_include(response):
    """Verifies the response to a request that includes an as-of-yet
    unsupported 'include' query parameter.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with an unsupported 'include' query
            parameter.
    """

    return await assert_unsupported_query(response, 'include')


async def assert_fields_invalid_resource(response, resource):
    """Verifies the response to a request that includes a 'fields' query with
    a nonexistent resource.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request that includes a 'fields' query with a
            nonexistent resource.
    """

    assert response.status == 400
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '400'
    assert error['title'] == 'Invalid query parameter.'
    assert error['detail'] == f"Resource '{resource}' does not exist."
    assert error['source']['parameter'] == 'fields'


async def assert_fields_invalid_field(response, resource, field):
    """Verifies the response to a request that includes a 'fields' query with
    a nonexistent resource.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request that includes a 'fields' query with a
            nonexistent resource.
    """

    assert response.status == 400
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '400'
    assert error['title'] == 'Invalid query parameter.'
    assert error['detail'] == (f"Field '{field}' does not exist in "
                               f"resource '{resource}'.")
    assert error['source']['parameter'] == 'fields'


async def assert_query_fields(response):
    """Verifies the response to a request that includes an as-of-yet
    unsupported 'fields' query parameter.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with an unsupported 'fields' query
            parameter.
    """

    return await assert_unsupported_query(response, 'fields')


async def assert_filter_invalid_path(response, path):
    """Verifies the response to a request that includes a 'filter' query with
    a nonexistent path.

    Args:
        response: Response to the failed request.
        field: Name of the invalid path.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request that includes a 'filter' query parameter
            with a nonexistent resource value.
    """

    assert response.status == 400
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '400'
    assert error['title'] == 'Invalid query parameter.'
    assert error['detail'] == f"The resource does not have a '{path}' path."
    assert error['source']['parameter'] == 'filter'


async def assert_filter_invalid_field(response, field):
    """Verifies the response to a request that includes a 'filter' query with
    a nonexistent field.

    Args:
        response: Response to the failed request.
        field: Name of the invalid field.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request that includes a 'filter' query parameter
            with a nonexistent resource value.
    """

    assert response.status == 400
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '400'
    assert error['title'] == 'Invalid query parameter.'
    assert error['detail'] == f"The resource does not have a '{field}' field."
    assert error['source']['parameter'] == 'filter'


async def assert_query_filter(response):
    """Verifies the response to a request that includes an as-of-yet
    unsupported 'filter' query parameter.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with an unsupported 'filter' query parameter.
    """

    return await assert_unsupported_query(response, 'filter')


async def assert_sort_invalid_field(response, field):
    """Verifies the response to a request that includes a 'sort' query with
    a nonexistent field.

    Args:
        response: Response to the failed request.
        field: Name of the invalid field.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request that includes a 'sort' query parameter
            with a nonexistent resource value.
    """

    assert response.status == 400
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '400'
    assert error['title'] == 'Invalid query parameter.'
    assert error['detail'] == f"The resource does not have a '{field}' field."
    assert error['source']['parameter'] == 'sort'


async def assert_query_sort(response):
    """Verifies the response to a request that includes an as-of-yet
    unsupported 'sort' query parameter.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with an unsupported 'sort' query parameter.
    """

    return await assert_unsupported_query(response, 'sort')


async def assert_query_page(response):
    """Verifies the response to a request that includes an as-of-yet
    unsupported 'page' query parameter.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response to a request with an unsupported 'page' query parameter.
    """

    return await assert_unsupported_query(response, 'page')


async def assert_400(response):
    """Verifies the response status is 400.

    Args:
        response: Response to the failed request.

    Returns:
        A JSON representation of the response document.

    Raises:
        AssertError: The response does not represent a JSON API errors
            response with a response status of 400.
    """

    assert response.status == 400
    return await assert_errors(response)
