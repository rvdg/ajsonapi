# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts.get_object provides functions to test the GET
/{collection}/{id} responses from the JSON API application.
"""

from ajsonapi.functests.asserts.generic import assert_object
from ajsonapi.functests.asserts.model import (
    assert_centers_id_related,
    assert_centers_id_unrelated,
    assert_centers_uuid_1_related,
    assert_centers_uuid_1_unrelated,
)


#
# Successful requests/responses
#
async def assert_get_object(response):
    """Verifies the response to a successful GET /{collection}/{id} request.
    """

    return await assert_object(response)


async def assert_get_centers_uuid_1_unrelated(response):
    """Verifies the response to a successful GET /centers/{UUID_1} request,
    where the resulting /centers/{UUID_1} object has no relationships.
    """

    document = await assert_object(response)
    data = document['data']
    assert_centers_uuid_1_unrelated(data)
    return document


async def assert_get_centers_uuid_1_related(response):
    """Verifies the response to a successful GET /centers/{UUID_1} request,
    where the resulting /centers/{UUID_1} object has all its functional test
    relationships in place.
    """

    document = await assert_object(response)
    data = document['data']
    assert_centers_uuid_1_related(data)
    return document


async def assert_get_centers_id_unrelated(response, id_=None):
    """Verifies the response to a successful GET /centers/{id} request, where
    the resulting /centers/{id} object has all its functional test relationships
    in place.
    """

    document = await assert_object(response)
    data = document['data']
    assert_centers_id_unrelated(data, id_)
    return document


async def assert_get_centers_id_related(response, id_=None):
    """Verifies the response to a successful GET /centers/{id} request, where
    the resulting /centers/{id} object has all its functional test relationships
    in place.
    """

    document = await assert_object(response)
    data = document['data']
    assert_centers_id_related(data, id_)
    return document
