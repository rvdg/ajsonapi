# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts.get_relationships provides functions to test the GET
/{collection}/{id}/relationships/{relationship} responses from the JSON API
application.
"""

from ajsonapi.functests.asserts.generic import assert_data_object
from ajsonapi.functests.asserts.get_collection import assert_get_collection
from ajsonapi.functests.headers import HEADERS
from ajsonapi.functests.model_objects import (
    UUID_1,
    UUID_11,
    UUID_21,
    UUID_31,
    UUID_41,
    UUID_42,
    UUID_43,
    UUID_51,
    UUID_52,
    UUID_53,
)


#
# Successful requests/responses
#
async def assert_get_to_one_relationship(response):
    """Verifies the response to a successful GET
    /{collection}/{id}/relationships/{relationship} request where the
    relationship is a to-one relationship.
    """

    assert response.status == 200
    assert response.headers['Content-Type'] == 'application/vnd.api+json'
    document = await response.json()
    assert isinstance(document, dict)
    assert 'data' in document
    assert 'links' in document

    obj = document['data']
    if obj is not None:
        assert_data_object(obj)

    links = document['links']
    assert isinstance(links, dict)
    assert 'related' in links
    assert 'self' in links
    url = response.url
    assert links['self'] in [url, url.path]

    return document


async def assert_get_to_many_relationship(response):
    """Verifies a response to a successful GET
    /{collection}/{id}/relationships/{relationship} request where the
    relationship is a to-many relationship.
    """
    document = await assert_get_collection(response)
    links = document['links']
    assert 'related' in links

    return document


#
# one_one_locals
#
async def assert_centers_uuid_1_one_one_local_none(client):
    """Verifies that the one_one_local relationship from centers UUID_1 is
    empty.
    """
    url = f'/resources/centers/{UUID_1}/relationships/one_one_local'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj is None
        assert 'included' not in document


async def assert_centers_uuid_1_one_one_local_uuid_11(client):
    """Verifies that the one_one_local relationship from centers UUID_1
    contains one_one_locals UUID_11.
    """
    url = f'/resources/centers/{UUID_1}/relationships/one_one_local'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj == {'type': 'one_one_locals', 'id': UUID_11}
        assert 'included' not in document


async def assert_one_one_locals_uuid_11_center_uuid_1(client):
    """Verifies that the center relationship from one_one_locals UUID_11
    contains centers UUID_1.
    """
    url = f'/resources/one_one_locals/{UUID_11}/relationships/center'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj == {'type': 'centers', 'id': UUID_1}
        assert 'included' not in document


#
# one_one_remotes
#
async def assert_centers_uuid_1_one_one_remote_none(client):
    """Verifies that the one_one_remote relationship from centers UUID_1 is
    empty.
    """
    url = f'/resources/centers/{UUID_1}/relationships/one_one_remote'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj is None
        assert 'included' not in document


async def assert_centers_uuid_1_one_one_remote_uuid_21(client):
    """Verifies that the one_one_remote relationship from centers UUID_1
    contains one_one_remotes UUID_21.
    """
    url = f'/resources/centers/{UUID_1}/relationships/one_one_remote'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj == {'type': 'one_one_remotes', 'id': UUID_21}
        assert 'included' not in document


async def assert_one_one_remotes_uuid_21_center_uuid_1(client):
    """Verifies that the center relationship from one_one_remotes UUID_21
    contains centers UUID_1.
    """
    url = f'/resources/one_one_remotes/{UUID_21}/relationships/center'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj == {'type': 'centers', 'id': UUID_1}
        assert 'included' not in document


#
# many_ones
#
async def assert_centers_uuid_1_many_one_none(client):
    """Verifies that the many_one relationship from centers UUID_1 is empty."""
    url = f'/resources/centers/{UUID_1}/relationships/many_one'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj is None
        assert 'included' not in document


async def assert_centers_uuid_1_many_one_uuid_31(client):
    """Verifies that the many_one relationship from centers UUID_1
    contains many_ones UUID_31.
    """
    url = f'/resources/centers/{UUID_1}/relationships/many_one'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj == {'type': 'many_ones', 'id': UUID_31}
        assert 'included' not in document


async def assert_many_ones_uuid_31_centers_uuid_1(client):
    """Verifies that the center relationship from many_ones UUID_31
    contains centers UUID_1.
    """
    url = f'/resources/many_ones/{UUID_31}/relationships/centers'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert data == [{'type': 'centers', 'id': UUID_1}]
        assert 'included' not in document


#
# one_manys
#
async def assert_centers_uuid_1_one_manys_none(client):
    """Verifies that the one_many relationship from centers UUID_1 is empty."""
    url = f'/resources/centers/{UUID_1}/relationships/one_manys'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert data == []
        assert 'included' not in document


async def assert_centers_uuid_1_one_manys_uuid_41(client):
    """Verifies that the one_manys relationship from centers UUID_1
    contains one_manys UUID_41.
    """
    url = f'/resources/centers/{UUID_1}/relationships/one_manys'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert data == [{'type': 'one_manys', 'id': UUID_41}]
        assert 'included' not in document


async def assert_centers_uuid_1_one_manys_uuid_41_42(client):
    """Verifies that the one_manys relationship from centers UUID_1
    contains one_manys UUID_41 and one_manys UUID_42.
    """
    url = f'/resources/centers/{UUID_1}/relationships/one_manys'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert len(data) == 2
        assert {'type': 'one_manys', 'id': UUID_41} in data
        assert {'type': 'one_manys', 'id': UUID_42} in data
        assert 'included' not in document


async def assert_centers_uuid_1_one_manys_uuid_41_42_43(client):
    """Verifies that the one_manys relationship from centers UUID_1
    contains one_manys UUID_41 and one_manys UUID_42.
    """
    url = f'/resources/centers/{UUID_1}/relationships/one_manys'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert len(data) == 3
        assert {'type': 'one_manys', 'id': UUID_41} in data
        assert {'type': 'one_manys', 'id': UUID_42} in data
        assert {'type': 'one_manys', 'id': UUID_43} in data
        assert 'included' not in document


async def assert_one_manys_uuid_41_center_uuid_1(client):
    """Verifies that the center relationship from one_manys UUID_41
    contains centers UUID_1.
    """
    url = f'/resources/one_manys/{UUID_41}/relationships/center'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj == {'type': 'centers', 'id': UUID_1}
        assert 'included' not in document


async def assert_one_manys_uuid_42_center_uuid_1(client):
    """Verifies that the center relationship from one_manys UUID_42
    contains centers UUID_1.
    """
    url = f'/resources/one_manys/{UUID_42}/relationships/center'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj == {'type': 'centers', 'id': UUID_1}
        assert 'included' not in document


async def assert_one_manys_uuid_43_center_uuid_1(client):
    """Verifies that the center relationship from one_manys UUID_43
    contains centers UUID_1.
    """
    url = f'/resources/one_manys/{UUID_43}/relationships/center'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_one_relationship(response)
        obj = document['data']
        assert obj == {'type': 'centers', 'id': UUID_1}
        assert 'included' not in document


#
# many_manys
#
async def assert_centers_uuid_1_many_manys_none(client):
    """Verifies that the many_many relationship from centers UUID_1 is empty."""
    url = f'/resources/centers/{UUID_1}/relationships/many_manys'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert data == []
        assert 'included' not in document


async def assert_centers_uuid_1_many_manys_uuid_51(client):
    """Verifies that the many_manys relationship from centers UUID_1
    contains many_manys UUID_51.
    """
    url = f'/resources/centers/{UUID_1}/relationships/many_manys'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert data == [{'type': 'many_manys', 'id': UUID_51}]
        assert 'included' not in document


async def assert_centers_uuid_1_many_manys_uuid_51_52(client):
    """Verifies that the many_manys relationship from centers UUID_1
    contains many_manys UUID_51 and many_manys UUID_52.
    """
    url = f'/resources/centers/{UUID_1}/relationships/many_manys'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert len(data) == 2
        assert {'type': 'many_manys', 'id': UUID_51} in data
        assert {'type': 'many_manys', 'id': UUID_52} in data
        assert 'included' not in document


async def assert_centers_uuid_1_many_manys_uuid_51_52_53(client):
    """Verifies that the many_manys relationship from centers UUID_1
    contains many_manys UUID_51 and many_manys UUID_52.
    """
    url = f'/resources/centers/{UUID_1}/relationships/many_manys'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert len(data) == 3
        assert {'type': 'many_manys', 'id': UUID_51} in data
        assert {'type': 'many_manys', 'id': UUID_52} in data
        assert {'type': 'many_manys', 'id': UUID_53} in data
        assert 'included' not in document


async def assert_many_manys_uuid_51_centers_uuid_1(client):
    """Verifies that the center relationship from many_manys UUID_51
    contains centers UUID_1.
    """
    url = f'/resources/many_manys/{UUID_51}/relationships/centers'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert data == [{'type': 'centers', 'id': UUID_1}]
        assert 'included' not in document


async def assert_many_manys_uuid_52_centers_uuid_1(client):
    """Verifies that the center relationship from many_manys UUID_52
    contains centers UUID_1.
    """
    url = f'/resources/many_manys/{UUID_52}/relationships/centers'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert data == [{'type': 'centers', 'id': UUID_1}]
        assert 'included' not in document


async def assert_many_manys_uuid_53_centers_uuid_1(client):
    """Verifies that the center relationship from many_manys UUID_52
    contains centers UUID_1.
    """
    url = f'/resources/many_manys/{UUID_53}/relationships/centers'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_to_many_relationship(response)
        data = document['data']
        assert data == [{'type': 'centers', 'id': UUID_1}]
        assert 'included' not in document
