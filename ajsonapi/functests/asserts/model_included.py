# Copyright © 2019-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts/model_included provides functions to test included resources
of the test models.
"""

# pylint: disable=too-many-lines

from ajsonapi.functests.model_objects import (
    UUID_1,
    UUID_2,
    UUID_11,
    UUID_14,
    UUID_21,
    UUID_24,
    UUID_31,
    UUID_34,
    UUID_41,
    UUID_42,
    UUID_44,
    UUID_45,
    UUID_51,
    UUID_52,
    UUID_54,
    UUID_55,
    UUID_111,
    UUID_121,
    UUID_131,
    UUID_141,
    UUID_142,
    UUID_151,
    UUID_152,
    UUID_211,
    UUID_221,
    UUID_231,
    UUID_241,
    UUID_242,
    UUID_251,
    UUID_252,
    UUID_311,
    UUID_321,
    UUID_331,
    UUID_341,
    UUID_342,
    UUID_351,
    UUID_352,
    UUID_411,
    UUID_421,
    UUID_431,
    UUID_441,
    UUID_442,
    UUID_451,
    UUID_452,
    UUID_511,
    UUID_521,
    UUID_531,
    UUID_541,
    UUID_542,
    UUID_551,
    UUID_552,
)


def assert_small_model_included_object_one_one_locals_uuid_11(obj, fields=None):
    """Asserts that an included /one_one_locals/{UUID_11} object from the
    small model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'ool_attr_int', 'ool_attr_str', 'center', 'ool_one_one_local',
            'ool_one_one_remote', 'ool_many_one', 'ool_one_manys',
            'ool_many_manys'
        }
    assert obj['type'] == 'one_one_locals'
    assert obj['id'] == UUID_11
    attributes = obj.get('attributes', {})
    if 'ool_attr_int' in fields:
        assert attributes['ool_attr_int'] == 211
    else:
        assert 'ool_attr_int' not in attributes
    if 'ool_attr_str' in fields:
        assert attributes['ool_attr_str'] == '11L-one'
    else:
        assert 'ool_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_1,
            },
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/center'),
                'related': f'/resources/one_one_locals/{UUID_11}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'ool_one_one_local' in fields:
        assert relationships['ool_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/ool_one_one_local'),
                'related': (f'/resources/one_one_locals/{UUID_11}'
                            '/ool_one_one_local'),
            }
        }
    else:
        assert 'ool_one_one_local' not in relationships
    if 'ool_one_one_remote' in fields:
        assert relationships['ool_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/ool_one_one_remote'),
                'related': (f'/resources/one_one_locals/{UUID_11}'
                            '/ool_one_one_remote'),
            }
        }
    else:
        assert 'ool_one_one_remote' not in relationships
    if 'ool_many_one' in fields:
        assert relationships['ool_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/ool_many_one'),
                'related': f'/resources/one_one_locals/{UUID_11}/ool_many_one',
            }
        }
    else:
        assert 'ool_many_one' not in relationships
    if 'ool_one_manys' in fields:
        assert relationships['ool_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/ool_one_manys'),
                'related': f'/resources/one_one_locals/{UUID_11}/ool_one_manys',
            }
        }
    else:
        assert 'ool_one_manys' not in relationships
    if 'ool_many_manys' in fields:
        assert relationships['ool_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/ool_many_manys'),
                'related': (f'/resources/one_one_locals/{UUID_11}'
                            '/ool_many_manys'),
            }
        }
    else:
        assert 'ool_many_manys' not in relationships


def assert_small_model_included_object_one_one_remotes_uuid_21(
        obj, fields=None):
    """Asserts that an included /one_one_remotes/{UUID_21} object from the
    small model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'oor_attr_int', 'oor_attr_str', 'center', 'oor_one_one_local',
            'oor_one_one_remote', 'oor_many_one', 'oor_one_manys',
            'oor_many_manys'
        }
    assert obj['type'] == 'one_one_remotes'
    assert obj['id'] == UUID_21
    attributes = obj.get('attributes', {})
    if 'oor_attr_int' in fields:
        assert attributes['oor_attr_int'] == 121
    else:
        assert 'oor_attr_int' not in attributes
    if 'oor_attr_str' in fields:
        assert attributes['oor_attr_str'] == '11R-one'
    else:
        assert 'oor_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_1,
            },
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/center'),
                'related': f'/resources/one_one_remotes/{UUID_21}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'oor_one_one_local' in fields:
        assert relationships['oor_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_one_one_local'),
                'related': (f'/resources/one_one_remotes/{UUID_21}'
                            '/oor_one_one_local'),
            }
        }
    else:
        assert 'oor_one_one_local' not in relationships
    if 'oor_one_one_remote' in fields:
        assert relationships['oor_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_one_one_remote'),
                'related': (f'/resources/one_one_remotes/{UUID_21}'
                            '/oor_one_one_remote'),
            }
        }
    else:
        assert 'oor_one_one_remote' not in relationships
    if 'oor_many_one' in fields:
        assert relationships['oor_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_many_one'),
                'related': f'/resources/one_one_remotes/{UUID_21}/oor_many_one',
            }
        }
    else:
        assert 'oor_many_one' not in relationships
    if 'oor_one_manys' in fields:
        assert relationships['oor_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_one_manys'),
                'related': (f'/resources/one_one_remotes/{UUID_21}'
                            '/oor_one_manys'),
            }
        }
    else:
        assert 'oor_one_manys' not in relationships
    if 'oor_many_manys' in fields:
        assert relationships['oor_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_many_manys'),
                'related': (f'/resources/one_one_remotes/{UUID_21}'
                            '/oor_many_manys'),
            }
        }
    else:
        assert 'oor_many_manys' not in relationships


def assert_small_model_included_object_many_ones_uuid_31(obj, fields=None):
    """Asserts that an included /many_ones/{UUID_31} object from the small
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'mo_attr_int', 'mo_attr_str', 'centers', 'mo_one_one_local',
            'mo_one_one_remote', 'mo_many_one', 'mo_one_manys', 'mo_many_manys'
        }
    assert obj['type'] == 'many_ones'
    assert obj['id'] == UUID_31
    attributes = obj.get('attributes', {})
    if 'mo_attr_int' in fields:
        assert attributes['mo_attr_int'] == 811
    else:
        assert 'mo_attr_int' not in attributes
    if 'mo_attr_str' in fields:
        assert attributes['mo_attr_str'] == 'M1-one'
    else:
        assert 'mo_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'centers' in fields:
        assert relationships['centers'] == {
            'data': [{
                'type': 'centers',
                'id': UUID_1,
            }],
            'links': {
                'self': f'/resources/many_ones/{UUID_31}/relationships/centers',
                'related': f'/resources/many_ones/{UUID_31}/centers',
            }
        }
    else:
        assert 'centers' not in relationships
    if 'mo_one_one_local' in fields:
        assert relationships['mo_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_one_one_local'),
                'related': f'/resources/many_ones/{UUID_31}/mo_one_one_local',
            }
        }
    else:
        assert 'mo_one_one_local' not in relationships
    if 'mo_one_one_remote' in fields:
        assert relationships['mo_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_one_one_remote'),
                'related': f'/resources/many_ones/{UUID_31}/mo_one_one_remote',
            }
        }
    else:
        assert 'mo_one_one_remote' not in relationships
    if 'mo_many_one' in fields:
        assert relationships['mo_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_many_one'),
                'related': f'/resources/many_ones/{UUID_31}/mo_many_one',
            }
        }
    else:
        assert 'mo_many_one' not in relationships
    if 'mo_one_manys' in fields:
        assert relationships['mo_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_one_manys'),
                'related': f'/resources/many_ones/{UUID_31}/mo_one_manys',
            }
        }
    else:
        assert 'mo_one_manys' not in relationships
    if 'mo_many_manys' in fields:
        assert relationships['mo_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_many_manys'),
                'related': f'/resources/many_ones/{UUID_31}/mo_many_manys',
            }
        }
    else:
        assert 'mo_many_manys' not in relationships


def assert_small_model_included_object_one_manys_uuid_41(obj, fields=None):
    """Asserts that an included /one_manys/{UUID_41} object from the small
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'om_attr_int', 'om_attr_str', 'center', 'om_one_one_local',
            'om_one_one_remote', 'om_many_one', 'om_one_manys', 'om_many_manys'
        }
    assert obj['type'] == 'one_manys'
    assert obj['id'] == UUID_41
    attributes = obj.get('attributes', {})
    if 'om_attr_int' in fields:
        assert attributes['om_attr_int'] == 181
    else:
        assert 'om_attr_int' not in attributes
    if 'om_attr_str' in fields:
        assert attributes['om_attr_str'] == '1M-one'
    else:
        assert 'om_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in relationships:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_1,
            },
            'links': {
                'self': f'/resources/one_manys/{UUID_41}/relationships/center',
                'related': f'/resources/one_manys/{UUID_41}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'om_one_one_local' in fields:
        assert relationships['om_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_one_one_local'),
                'related': f'/resources/one_manys/{UUID_41}/om_one_one_local',
            }
        }
    else:
        assert 'center' not in relationships
    if 'om_one_one_remote' in fields:
        assert relationships['om_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_one_one_remote'),
                'related': f'/resources/one_manys/{UUID_41}/om_one_one_remote',
            }
        }
    else:
        assert 'om_one_one_remote' not in relationships
    if 'om_many_one' in fields:
        assert relationships['om_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_many_one'),
                'related': f'/resources/one_manys/{UUID_41}/om_many_one',
            }
        }
    else:
        assert 'om_many_one' not in relationships
    if 'om_one_manys' in fields:
        assert relationships['om_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_one_manys'),
                'related': f'/resources/one_manys/{UUID_41}/om_one_manys',
            }
        }
    else:
        assert 'om_one_manys' not in relationships
    if 'om_many_manys' in fields:
        assert relationships['om_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_many_manys'),
                'related': f'/resources/one_manys/{UUID_41}/om_many_manys',
            }
        }
    else:
        assert 'om_many_manys' not in relationships


def assert_small_model_included_object_one_manys_uuid_42(obj, fields=None):
    """Asserts that an included /one_manys/{UUID_42} object from the small
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'om_attr_int', 'om_attr_str', 'center', 'om_one_one_local',
            'om_one_one_remote', 'om_many_one', 'om_one_manys', 'om_many_manys'
        }
    assert obj['type'] == 'one_manys'
    assert obj['id'] == UUID_42
    attributes = obj.get('attributes', {})
    if 'om_attr_int' in fields:
        assert attributes['om_attr_int'] == 182
    else:
        assert 'om_attr_int' not in attributes
    if 'om_attr_str' in fields:
        assert attributes['om_attr_str'] == '1M-two'
    else:
        assert 'om_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_1,
            },
            'links': {
                'self': f'/resources/one_manys/{UUID_42}/relationships/center',
                'related': f'/resources/one_manys/{UUID_42}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'om_one_one_local' in fields:
        assert relationships['om_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_42}'
                         '/relationships/om_one_one_local'),
                'related': f'/resources/one_manys/{UUID_42}/om_one_one_local',
            }
        }
    else:
        assert 'om_one_one_local' not in relationships
    if 'om_one_one_remote' in fields:
        assert relationships['om_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_42}'
                         '/relationships/om_one_one_remote'),
                'related': f'/resources/one_manys/{UUID_42}/om_one_one_remote',
            }
        }
    else:
        assert 'om_one_one_remote' not in relationships
    if 'om_many_one' in fields:
        assert relationships['om_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_42}'
                         '/relationships/om_many_one'),
                'related': f'/resources/one_manys/{UUID_42}/om_many_one',
            }
        }
    else:
        assert 'om_many_one' not in relationships
    if 'om_one_manys' in fields:
        assert relationships['om_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_manys/{UUID_42}'
                         '/relationships/om_one_manys'),
                'related': f'/resources/one_manys/{UUID_42}/om_one_manys',
            }
        }
    else:
        assert 'om_one_manys' not in relationships
    if 'om_many_manys' in fields:
        assert relationships['om_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_manys/{UUID_42}'
                         '/relationships/om_many_manys'),
                'related': f'/resources/one_manys/{UUID_42}/om_many_manys',
            }
        }
    else:
        assert 'om_many_manys' not in relationships


def assert_small_model_included_object_many_manys_uuid_51(obj, fields=None):
    """Asserts that an included /many_manys/{UUID_51} object from the small
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'mm_attr_int', 'mm_attr_str', 'centers', 'mm_one_one_local',
            'mm_one_one_remote', 'mm_many_one', 'mm_one_manys', 'mm_many_manys'
        }
    assert obj['type'] == 'many_manys'
    assert obj['id'] == UUID_51
    attributes = obj.get('attributes', {})
    if 'mm_attr_int' in fields:
        assert attributes['mm_attr_int'] == 881
    else:
        assert 'mm_attr_int' not in attributes
    if 'mm_attr_str' in fields:
        assert attributes['mm_attr_str'] == 'MM-one'
    else:
        assert 'mm_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'centers' in fields:
        assert relationships['centers'] == {
            'data': [{
                'type': 'centers',
                'id': UUID_1,
            }],
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/centers'),
                'related': f'/resources/many_manys/{UUID_51}/centers',
            }
        }
    else:
        assert 'centers' not in relationships
    if 'mm_one_one_local' in fields:
        assert relationships['mm_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_one_one_local'),
                'related': f'/resources/many_manys/{UUID_51}/mm_one_one_local',
            }
        }
    else:
        assert 'mm_one_one_local' not in relationships
    if 'mm_one_one_remote' in fields:
        assert relationships['mm_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_one_one_remote'),
                'related': f'/resources/many_manys/{UUID_51}/mm_one_one_remote',
            }
        }
    else:
        assert 'mm_one_one_remote' not in relationships
    if 'mm_many_one' in fields:
        assert relationships['mm_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_many_one'),
                'related': f'/resources/many_manys/{UUID_51}/mm_many_one',
            }
        }
    else:
        assert 'mm_many_one' not in relationships
    if 'mm_one_manys' in fields:
        assert relationships['mm_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_one_manys'),
                'related': f'/resources/many_manys/{UUID_51}/mm_one_manys',
            }
        }
    else:
        assert 'mm_one_manys' not in relationships
    if 'mm_many_manys' in fields:
        assert relationships['mm_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_many_manys'),
                'related': f'/resources/many_manys/{UUID_51}/mm_many_manys',
            }
        }
    else:
        assert 'mm_many_manys' not in relationships


def assert_small_model_included_object_many_manys_uuid_52(obj, fields=None):
    """Asserts that an included /many_manys/{UUID_52} object from the small
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'mm_attr_int', 'mm_attr_str', 'centers', 'mm_one_one_local',
            'mm_one_one_remote', 'mm_many_one', 'mm_one_manys', 'mm_many_manys'
        }
    assert obj['type'] == 'many_manys'
    assert obj['id'] == UUID_52
    attributes = obj.get('attributes', {})
    if 'mm_attr_int' in fields:
        assert attributes['mm_attr_int'] == 882
    else:
        assert 'mm_attr_int' not in attributes
    if 'mm_attr_str' in fields:
        assert attributes['mm_attr_str'] == 'MM-two'
    else:
        assert 'mm_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'centers' in fields:
        assert relationships['centers'] == {
            'data': [{
                'type': 'centers',
                'id': UUID_1,
            }],
            'links': {
                'self': (f'/resources/many_manys/{UUID_52}'
                         '/relationships/centers'),
                'related': f'/resources/many_manys/{UUID_52}/centers',
            }
        }
    else:
        assert 'centers' not in relationships
    if 'mm_one_one_local' in fields:
        assert relationships['mm_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_52}'
                         '/relationships/mm_one_one_local'),
                'related': f'/resources/many_manys/{UUID_52}/mm_one_one_local',
            }
        }
    else:
        assert 'mm_one_one_local' not in relationships
    if 'mm_one_one_remote' in fields:
        assert relationships['mm_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_52}'
                         '/relationships/mm_one_one_remote'),
                'related': f'/resources/many_manys/{UUID_52}/mm_one_one_remote',
            }
        }
    else:
        assert 'mm_one_one_remote' not in relationships
    if 'mm_many_one' in fields:
        assert relationships['mm_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_52}'
                         '/relationships/mm_many_one'),
                'related': f'/resources/many_manys/{UUID_52}/mm_many_one',
            }
        }
    else:
        assert 'mm_many_one' not in relationships
    if 'mm_one_manys' in fields:
        assert relationships['mm_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_manys/{UUID_52}'
                         '/relationships/mm_one_manys'),
                'related': f'/resources/many_manys/{UUID_52}/mm_one_manys',
            }
        }
    else:
        assert 'mm_one_manys' not in relationships
    if 'mm_many_manys' in fields:
        assert relationships['mm_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_manys/{UUID_52}'
                         '/relationships/mm_many_manys'),
                'related': f'/resources/many_manys/{UUID_52}/mm_many_manys',
            }
        }
    else:
        assert 'mm_many_manys' not in relationships


def assert_small_model_included(included, content_by_type):
    """Asserts that all resource identification objects are represented
    correctly in included.
    """
    function_by_rio = {
        ('one_one_locals', UUID_11):
            assert_small_model_included_object_one_one_locals_uuid_11,
        ('one_one_remotes', UUID_21):
            assert_small_model_included_object_one_one_remotes_uuid_21,
        ('many_ones', UUID_31):
            assert_small_model_included_object_many_ones_uuid_31,
        ('one_manys', UUID_41):
            assert_small_model_included_object_one_manys_uuid_41,
        ('one_manys', UUID_42):
            assert_small_model_included_object_one_manys_uuid_42,
        ('many_manys', UUID_51):
            assert_small_model_included_object_many_manys_uuid_51,
        ('many_manys', UUID_52):
            assert_small_model_included_object_many_manys_uuid_52
    }

    obj_by_rio = {(obj['type'], obj['id']): obj for obj in included}
    rios = {(obj_type, obj_id)
            for obj_type, content in content_by_type.items()
            for obj_id in content[0]}
    assert obj_by_rio.keys() == rios
    fields = {
        obj_type: content[1] for obj_type, content in content_by_type.items()
    }
    for rio, obj in obj_by_rio.items():
        function_by_rio[rio](obj, fields[rio[0]])


def assert_medium_model_included_object_centers_uuid_1(obj, fields=None):
    """Asserts that an included /centers/{UUID_1} object from the medium model
    is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'attr_int', 'attr_str', 'one_one_local', 'one_one_remote',
            'many_one', 'one_manys', 'many_manys'
        }
    assert obj['type'] == 'centers'
    assert obj['id'] == UUID_1
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 1
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_local' in fields:
        assert relationships['one_one_local']['data'] == {
            'type': 'one_one_locals',
            'id': UUID_11,
        }
    else:
        assert 'one_one_local' not in relationships
    if 'one_one_remote' in fields:
        assert relationships['one_one_remote']['data'] == {
            'type': 'one_one_remotes',
            'id': UUID_21,
        }
    else:
        assert 'one_one_remote' not in relationships
    if 'many_one' in fields:
        assert relationships['many_one']['data'] == {
            'type': 'many_ones',
            'id': UUID_31,
        }
    else:
        assert 'many_ones' not in relationships
    if 'one_manys' in fields:
        assert relationships['one_manys']['data'] == [
            {
                'type': 'one_manys',
                'id': UUID_41,
            },
            {
                'type': 'one_manys',
                'id': UUID_42,
            },
        ]
    else:
        assert 'one_manys' not in relationships
    if 'many_manys' in fields:
        assert relationships['many_manys']['data'] == [
            {
                'type': 'many_manys',
                'id': UUID_51,
            },
            {
                'type': 'many_manys',
                'id': UUID_52,
            },
        ]
    else:
        assert 'many_manys' not in relationships


def assert_medium_model_included_object_one_one_locals_uuid_11(
        obj, fields=None):
    """Asserts that an included /one_one_locals/{UUID_11} object from the
    medium model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'ool_attr_int', 'ool_attr_str', 'center', 'ool_one_one_local',
            'ool_one_one_remote', 'ool_many_one', 'ool_one_manys',
            'ool_many_manys'
        }
    assert obj['type'] == 'one_one_locals'
    assert obj['id'] == UUID_11
    attributes = obj.get('attributes', {})
    if 'ool_attr_int' in fields:
        assert attributes['ool_attr_int'] == 211
    else:
        assert 'ool_attr_int' not in attributes
    if 'ool_attr_str' in fields:
        assert attributes['ool_attr_str'] == '11L-one'
    else:
        assert 'ool_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_1,
            },
            'links': {
                'self': f'/resources/one_one_locals/{UUID_11}'
                        '/relationships/center',
                'related': f'/resources/one_one_locals/{UUID_11}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'ool_one_one_local' in fields:
        assert relationships['ool_one_one_local'] == {
            'data': {
                'type': 'ool_one_one_locals',
                'id': UUID_111,
            },
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/ool_one_one_local'),
                'related': f'/resources/one_one_locals/{UUID_11}'
                           '/ool_one_one_local',
            }
        }
    else:
        assert 'ool_one_one_local' not in relationships
    if 'ool_one_one_remote' in fields:
        assert relationships['ool_one_one_remote'] == {
            'data': {
                'type': 'ool_one_one_remotes',
                'id': UUID_121,
            },
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/ool_one_one_remote'),
                'related': f'/resources/one_one_locals/{UUID_11}'
                           '/ool_one_one_remote',
            }
        }
    else:
        assert 'ool_one_one_remote' not in relationships
    if 'ool_many_one' in fields:
        assert relationships['ool_many_one'] == {
            'data': {
                'type': 'ool_many_ones',
                'id': UUID_131,
            },
            'links': {
                'self': f'/resources/one_one_locals/{UUID_11}'
                        '/relationships/ool_many_one',
                'related': f'/resources/one_one_locals/{UUID_11}/ool_many_one',
            }
        }
    else:
        assert 'ool_many_ones' not in relationships
    if 'ool_one_manys' in fields:
        assert relationships['ool_one_manys'] == {
            'data': [
                {
                    'type': 'ool_one_manys',
                    'id': UUID_141,
                },
                {
                    'type': 'ool_one_manys',
                    'id': UUID_142,
                },
            ],
            'links': {
                'self': f'/resources/one_one_locals/{UUID_11}'
                        '/relationships/ool_one_manys',
                'related': f'/resources/one_one_locals/{UUID_11}/ool_one_manys',
            }
        }
    else:
        assert 'ool_one_manys' not in relationships
    if 'ool_many_manys' in fields:
        assert relationships['ool_many_manys'] == {
            'data': [
                {
                    'type': 'ool_many_manys',
                    'id': UUID_151,
                },
                {
                    'type': 'ool_many_manys',
                    'id': UUID_152,
                },
            ],
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_11}'
                         '/relationships/ool_many_manys'),
                'related': (f'/resources/one_one_locals/{UUID_11}'
                            '/ool_many_manys'),
            }
        }
    else:
        assert 'ool_many_manys' not in relationships


def assert_medium_model_included_object_one_one_locals_uuid_14(
        obj, fields=None):
    """Asserts that an included /one_one_locals/{UUID_14} object from the
    medium model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'ool_attr_int', 'ool_attr_str', 'center', 'ool_one_one_local',
            'ool_one_one_remote', 'ool_many_one', 'ool_one_manys',
            'ool_many_manys'
        }
    assert obj['type'] == 'one_one_locals'
    assert obj['id'] == UUID_14
    attributes = obj.get('attributes', {})
    if 'ool_attr_int' in fields:
        assert attributes['ool_attr_int'] == 214
    else:
        assert 'ool_attr_int' not in attributes
    if 'ool_attr_str' in fields:
        assert attributes['ool_attr_str'] == '11L-four'
    else:
        assert 'ool_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_2,
            },
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_14}'
                         '/relationships/center'),
                'related': f'/resources/one_one_locals/{UUID_14}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'ool_one_one_local' in fields:
        assert relationships['ool_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_14}'
                         '/relationships/ool_one_one_local'),
                'related': (f'/resources/one_one_locals/{UUID_14}'
                            '/ool_one_one_local'),
            }
        }
    else:
        assert 'ool_one_one_local' not in relationships
    if 'ool_one_one_remote' in fields:
        assert relationships['ool_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_14}'
                         '/relationships/ool_one_one_remote'),
                'related': (f'/resources/one_one_locals/{UUID_14}'
                            '/ool_one_one_remote'),
            }
        }
    else:
        assert 'ool_one_one_remote' not in relationships
    if 'ool_many_one' in fields:
        assert relationships['ool_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_14}'
                         '/relationships/ool_many_one'),
                'related': f'/resources/one_one_locals/{UUID_14}/ool_many_one',
            }
        }
    else:
        assert 'ool_many_ones' not in relationships
    if 'ool_one_manys' in fields:
        assert relationships['ool_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_14}'
                         '/relationships/ool_one_manys'),
                'related': f'/resources/one_one_locals/{UUID_14}/ool_one_manys',
            }
        }
    else:
        assert 'ool_one_manys' not in relationships
    if 'ool_many_manys' in fields:
        assert relationships['ool_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_one_locals/{UUID_14}'
                         '/relationships/ool_many_manys'),
                'related': (f'/resources/one_one_locals/{UUID_14}'
                            '/ool_many_manys'),
            }
        }
    else:
        assert 'ool_many_manys' not in relationships


def assert_medium_model_included_object_one_one_remotes_uuid_21(
        obj, fields=None):
    """Asserts that an included /one_one_remotes/{UUID_21} object from the
    medium model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'oor_attr_int', 'oor_attr_str', 'center', 'oor_one_one_local',
            'oor_one_one_remote', 'oor_many_one', 'oor_one_manys',
            'oor_many_manys'
        }
    assert obj['type'] == 'one_one_remotes'
    assert obj['id'] == UUID_21
    attributes = obj.get('attributes', {})
    if 'oor_attr_int' in fields:
        assert attributes['oor_attr_int'] == 121
    else:
        assert 'oor_attr_int' not in attributes
    if 'oor_attr_str' in fields:
        assert attributes['oor_attr_str'] == '11R-one'
    else:
        assert 'oor_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_1,
            },
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/center'),
                'related': f'/resources/one_one_remotes/{UUID_21}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'oor_one_one_local' in fields:
        assert relationships['oor_one_one_local'] == {
            'data': {
                'type': 'oor_one_one_locals',
                'id': UUID_211,
            },
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_one_one_local'),
                'related': (f'/resources/one_one_remotes/{UUID_21}'
                            '/oor_one_one_local'),
            }
        }
    else:
        assert 'oor_one_one_local' not in relationships
    if 'oor_one_one_remote' in fields:
        assert relationships['oor_one_one_remote'] == {
            'data': {
                'type': 'oor_one_one_remotes',
                'id': UUID_221,
            },
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_one_one_remote'),
                'related': (f'/resources/one_one_remotes/{UUID_21}'
                            '/oor_one_one_remote'),
            }
        }
    else:
        assert 'oor_one_one_remote' not in relationships
    if 'oor_many_one' in fields:
        assert relationships['oor_many_one'] == {
            'data': {
                'type': 'oor_many_ones',
                'id': UUID_231,
            },
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_many_one'),
                'related': f'/resources/one_one_remotes/{UUID_21}/oor_many_one',
            }
        }
    else:
        assert 'oor_many_one' not in relationships
    if 'oor_one_manys' in fields:
        assert relationships['oor_one_manys'] == {
            'data': [
                {
                    'type': 'oor_one_manys',
                    'id': UUID_241,
                },
                {
                    'type': 'oor_one_manys',
                    'id': UUID_242,
                },
            ],
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_one_manys'),
                'related': (f'/resources/one_one_remotes/{UUID_21}'
                            '/oor_one_manys'),
            }
        }
    else:
        assert 'oor_one_manys' not in relationships
    if 'oor_many_manys' in fields:
        assert relationships['oor_many_manys'] == {
            'data': [
                {
                    'type': 'oor_many_manys',
                    'id': UUID_251,
                },
                {
                    'type': 'oor_many_manys',
                    'id': UUID_252,
                },
            ],
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_21}'
                         '/relationships/oor_many_manys'),
                'related': (f'/resources/one_one_remotes/{UUID_21}'
                            '/oor_many_manys'),
            }
        }
    else:
        assert 'oor_many_manys' not in relationships


def assert_medium_model_included_object_one_one_remotes_uuid_24(
        obj, fields=None):
    """Asserts that an included /one_one_remotes/{UUID_24} object from the
    medium model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'oor_attr_int', 'oor_attr_str', 'center', 'oor_one_one_local',
            'oor_one_one_remote', 'oor_many_one', 'oor_one_manys',
            'oor_many_manys'
        }
    assert obj['type'] == 'one_one_remotes'
    assert obj['id'] == UUID_24
    attributes = obj.get('attributes', {})
    if 'oor_attr_int' in fields:
        assert attributes['oor_attr_int'] == 124
    else:
        assert 'oor_attr_int' not in attributes
    if 'oor_attr_str' in fields:
        assert attributes['oor_attr_str'] == '11R-four'
    else:
        assert 'oor_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_2,
            },
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_24}'
                         '/relationships/center'),
                'related': f'/resources/one_one_remotes/{UUID_24}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'oor_one_one_local' in fields:
        assert relationships['oor_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_24}'
                         '/relationships/oor_one_one_local'),
                'related': (f'/resources/one_one_remotes/{UUID_24}'
                            '/oor_one_one_local'),
            }
        }
    else:
        assert 'oor_one_one_local' not in relationships
    if 'oor_one_one_remote' in fields:
        assert relationships['oor_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_24}'
                         '/relationships/oor_one_one_remote'),
                'related': (f'/resources/one_one_remotes/{UUID_24}'
                            '/oor_one_one_remote'),
            }
        }
    else:
        assert 'oor_one_one_remote' not in relationships
    if 'oor_many_one' in fields:
        assert relationships['oor_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_24}'
                         '/relationships/oor_many_one'),
                'related': f'/resources/one_one_remotes/{UUID_24}/oor_many_one',
            }
        }
    else:
        assert 'oor_many_one' not in relationships
    if 'oor_one_manys' in fields:
        assert relationships['oor_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_24}'
                         '/relationships/oor_one_manys'),
                'related': (f'/resources/one_one_remotes/{UUID_24}'
                            '/oor_one_manys'),
            }
        }
    else:
        assert 'oor_one_manys' not in relationships
    if 'oor_many_manys' in fields:
        assert relationships['oor_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_one_remotes/{UUID_24}'
                         '/relationships/oor_many_manys'),
                'related': (f'/resources/one_one_remotes/{UUID_24}'
                            '/oor_many_manys'),
            }
        }
    else:
        assert 'oor_many_manys' not in relationships


def assert_medium_model_included_object_many_ones_uuid_31(obj, fields=None):
    """Asserts that an included /many_ones/{UUID_31} object from the medium
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'mo_attr_int', 'mo_attr_str', 'centers', 'mo_one_one_local',
            'mo_one_one_remote', 'mo_many_one', 'mo_one_manys', 'mo_many_manys'
        }
    assert obj['type'] == 'many_ones'
    assert obj['id'] == UUID_31
    attributes = obj.get('attributes', {})
    if 'mo_attr_int' in fields:
        assert attributes['mo_attr_int'] == 811
    else:
        assert 'mo_attr_int' not in attributes
    if 'mo_attr_str' in fields:
        assert attributes['mo_attr_str'] == 'M1-one'
    else:
        assert 'mo_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'centers' in fields:
        assert relationships['centers'] == {
            'data': [{
                'type': 'centers',
                'id': UUID_1,
            }],
            'links': {
                'self': f'/resources/many_ones/{UUID_31}/relationships/centers',
                'related': f'/resources/many_ones/{UUID_31}/centers',
            }
        }
    else:
        assert 'centers' not in relationships
    if 'mo_one_one_local' in fields:
        assert relationships['mo_one_one_local'] == {
            'data': {
                'type': 'mo_one_one_locals',
                'id': UUID_311,
            },
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_one_one_local'),
                'related': f'/resources/many_ones/{UUID_31}/mo_one_one_local',
            }
        }
    else:
        assert 'mo_one_one_local' not in relationships
    if 'mo_one_one_remote' in fields:
        assert relationships['mo_one_one_remote'] == {
            'data': {
                'type': 'mo_one_one_remotes',
                'id': UUID_321,
            },
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_one_one_remote'),
                'related': f'/resources/many_ones/{UUID_31}/mo_one_one_remote',
            }
        }
    else:
        assert 'mo_one_one_remote' not in relationships
    if 'mo_many_one' in fields:
        assert relationships['mo_many_one'] == {
            'data': {
                'type': 'mo_many_ones',
                'id': UUID_331,
            },
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_many_one'),
                'related': f'/resources/many_ones/{UUID_31}/mo_many_one',
            }
        }
    else:
        assert 'mo_many_one' not in relationships
    if 'mo_one_manys' in fields:
        assert relationships['mo_one_manys'] == {
            'data': [
                {
                    'type': 'mo_one_manys',
                    'id': UUID_341,
                },
                {
                    'type': 'mo_one_manys',
                    'id': UUID_342,
                },
            ],
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_one_manys'),
                'related': f'/resources/many_ones/{UUID_31}/mo_one_manys',
            }
        }
    else:
        assert 'mo_one_manys' not in relationships
    if 'mo_many_manys' in fields:
        assert relationships['mo_many_manys'] == {
            'data': [
                {
                    'type': 'mo_many_manys',
                    'id': UUID_351,
                },
                {
                    'type': 'mo_many_manys',
                    'id': UUID_352,
                },
            ],
            'links': {
                'self': (f'/resources/many_ones/{UUID_31}'
                         '/relationships/mo_many_manys'),
                'related': f'/resources/many_ones/{UUID_31}/mo_many_manys',
            }
        }
    else:
        assert 'mo_many_manys' not in relationships


def assert_medium_model_included_object_many_ones_uuid_34(obj, fields=None):
    """Asserts that an included /many_ones/{UUID_34} object from the medium
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'mo_attr_int', 'mo_attr_str', 'centers', 'mo_one_one_local',
            'mo_one_one_remote', 'mo_many_one', 'mo_one_manys', 'mo_many_manys'
        }
    assert obj['type'] == 'many_ones'
    assert obj['id'] == UUID_34
    attributes = obj.get('attributes', {})
    if 'mo_attr_int' in fields:
        assert attributes['mo_attr_int'] == 814
    else:
        assert 'mo_attr_int' not in attributes
    if 'mo_attr_str' in fields:
        assert attributes['mo_attr_str'] == 'M1-four'
    else:
        assert 'mo_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'centers' in fields:
        assert relationships['centers'] == {
            'data': [{
                'type': 'centers',
                'id': UUID_2,
            }],
            'links': {
                'self': f'/resources/many_ones/{UUID_34}/relationships/centers',
                'related': f'/resources/many_ones/{UUID_34}/centers',
            }
        }
    else:
        assert 'centers' not in relationships
    if 'mo_one_one_local' in fields:
        assert relationships['mo_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_ones/{UUID_34}'
                         '/relationships/mo_one_one_local'),
                'related': f'/resources/many_ones/{UUID_34}/mo_one_one_local',
            }
        }
    else:
        assert 'mo_one_one_local' not in relationships
    if 'mo_one_one_remote' in fields:
        assert relationships['mo_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_ones/{UUID_34}'
                         '/relationships/mo_one_one_remote'),
                'related': f'/resources/many_ones/{UUID_34}/mo_one_one_remote',
            }
        }
    else:
        assert 'mo_one_one_remote' not in relationships
    if 'mo_many_one' in fields:
        assert relationships['mo_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_ones/{UUID_34}'
                         '/relationships/mo_many_one'),
                'related': f'/resources/many_ones/{UUID_34}/mo_many_one',
            }
        }
    else:
        assert 'mo_many_one' not in relationships
    if 'mo_one_manys' in fields:
        assert relationships['mo_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_ones/{UUID_34}'
                         '/relationships/mo_one_manys'),
                'related': f'/resources/many_ones/{UUID_34}/mo_one_manys',
            }
        }
    else:
        assert 'mo_one_manys' not in relationships
    if 'mo_many_manys' in fields:
        assert relationships['mo_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_ones/{UUID_34}'
                         '/relationships/mo_many_manys'),
                'related': f'/resources/many_ones/{UUID_34}/mo_many_manys',
            }
        }
    else:
        assert 'mo_many_manys' not in relationships


def assert_medium_model_included_object_one_manys_uuid_41(obj, fields=None):
    """Asserts that an included /one_manys/{UUID_41} object from the medium
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'om_attr_int', 'om_attr_str', 'center', 'om_one_one_local',
            'om_one_one_remote', 'om_many_one', 'om_one_manys', 'om_many_manys'
        }
    assert obj['type'] == 'one_manys'
    assert obj['id'] == UUID_41
    attributes = obj.get('attributes', {})
    if 'om_attr_int' in fields:
        assert attributes['om_attr_int'] == 181
    else:
        assert 'om_attr_int' not in attributes
    if 'om_attr_str' in fields:
        assert attributes['om_attr_str'] == '1M-one'
    else:
        assert 'om_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_1,
            },
            'links': {
                'self': f'/resources/one_manys/{UUID_41}/relationships/center',
                'related': f'/resources/one_manys/{UUID_41}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'om_one_one_local' in fields:
        assert relationships['om_one_one_local'] == {
            'data': {
                'type': 'om_one_one_locals',
                'id': UUID_411,
            },
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_one_one_local'),
                'related': f'/resources/one_manys/{UUID_41}/om_one_one_local',
            }
        }
    else:
        assert 'om_one_one_local' not in relationships
    if 'om_one_one_remote' in fields:
        assert relationships['om_one_one_remote'] == {
            'data': {
                'type': 'om_one_one_remotes',
                'id': UUID_421,
            },
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_one_one_remote'),
                'related': f'/resources/one_manys/{UUID_41}/om_one_one_remote',
            }
        }
    else:
        assert 'om_one_one_remote' not in relationships
    if 'om_many_one' in fields:
        assert relationships['om_many_one'] == {
            'data': {
                'type': 'om_many_ones',
                'id': UUID_431,
            },
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_many_one'),
                'related': f'/resources/one_manys/{UUID_41}/om_many_one',
            }
        }
    else:
        assert 'om_many_one' not in relationships
    if 'om_one_manys' in fields:
        assert relationships['om_one_manys'] == {
            'data': [
                {
                    'type': 'om_one_manys',
                    'id': UUID_441,
                },
                {
                    'type': 'om_one_manys',
                    'id': UUID_442,
                },
            ],
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_one_manys'),
                'related': f'/resources/one_manys/{UUID_41}/om_one_manys',
            }
        }
    else:
        assert 'om_one_manys' not in relationships
    if 'om_many_manys' in fields:
        assert relationships['om_many_manys'] == {
            'data': [
                {
                    'type': 'om_many_manys',
                    'id': UUID_451,
                },
                {
                    'type': 'om_many_manys',
                    'id': UUID_452,
                },
            ],
            'links': {
                'self': (f'/resources/one_manys/{UUID_41}'
                         '/relationships/om_many_manys'),
                'related': f'/resources/one_manys/{UUID_41}/om_many_manys',
            }
        }
    else:
        assert 'om_many_manys' not in relationships


def assert_medium_model_included_object_one_manys_uuid_42(obj, fields=None):
    """Asserts that an included /one_manys/{UUID_42} object from the medium
    model is properly formatted.
    """
    assert_small_model_included_object_one_manys_uuid_42(obj, fields)


def assert_medium_model_included_object_one_manys_uuid_44(obj, fields=None):
    """Asserts that an included /one_manys/{UUID_44} object from the medium
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'om_attr_int', 'om_attr_str', 'center', 'om_one_one_local',
            'om_one_one_remote', 'om_many_one', 'om_one_manys', 'om_many_manys'
        }
    assert obj['type'] == 'one_manys'
    assert obj['id'] == UUID_44
    attributes = obj.get('attributes', {})
    if 'om_attr_int' in fields:
        assert attributes['om_attr_int'] == 184
    else:
        assert 'om_attr_int' not in attributes
    if 'om_attr_str' in fields:
        assert attributes['om_attr_str'] == '1M-four'
    else:
        assert 'om_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_2,
            },
            'links': {
                'self': f'/resources/one_manys/{UUID_44}/relationships/center',
                'related': f'/resources/one_manys/{UUID_44}/center'
            }
        }
    else:
        assert 'center' not in relationships
    if 'om_one_one_local' in fields:
        assert relationships['om_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_44}'
                         '/relationships/om_one_one_local'),
                'related': f'/resources/one_manys/{UUID_44}/om_one_one_local',
            }
        }
    else:
        assert 'om_one_one_local' not in relationships
    if 'om_one_one_remote' in fields:
        assert relationships['om_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_44}'
                         '/relationships/om_one_one_remote'),
                'related': f'/resources/one_manys/{UUID_44}/om_one_one_remote',
            }
        }
    else:
        assert 'om_one_one_remote' not in relationships
    if 'om_many_one' in fields:
        assert relationships['om_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_44}'
                         '/relationships/om_many_one'),
                'related': f'/resources/one_manys/{UUID_44}/om_many_one',
            }
        }
    else:
        assert 'om_many_one' not in relationships
    if 'om_one_manys' in fields:
        assert relationships['om_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_manys/{UUID_44}'
                         '/relationships/om_one_manys'),
                'related': f'/resources/one_manys/{UUID_44}/om_one_manys',
            }
        }
    else:
        assert 'om_one_manys' not in relationships
    if 'om_many_manys' in fields:
        assert relationships['om_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_manys/{UUID_44}'
                         '/relationships/om_many_manys'),
                'related': f'/resources/one_manys/{UUID_44}/om_many_manys',
            }
        }
    else:
        assert 'om_many_manys' not in relationships


def assert_medium_model_included_object_one_manys_uuid_45(obj, fields=None):
    """Asserts that an included /one_manys/{UUID_45} object from the medium
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'om_attr_int', 'om_attr_str', 'center', 'om_one_one_local',
            'om_one_one_remote', 'om_many_one', 'om_one_manys', 'om_many_manys'
        }
    assert obj['type'] == 'one_manys'
    assert obj['id'] == UUID_45
    attributes = obj.get('attributes', {})
    if 'om_attr_int' in fields:
        assert attributes['om_attr_int'] == 185
    else:
        assert 'om_attr_int' not in attributes
    if 'om_attr_str' in fields:
        assert attributes['om_attr_str'] == '1M-five'
    else:
        assert 'om_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'center' in fields:
        assert relationships['center'] == {
            'data': {
                'type': 'centers',
                'id': UUID_2,
            },
            'links': {
                'self': f'/resources/one_manys/{UUID_45}/relationships/center',
                'related': f'/resources/one_manys/{UUID_45}/center',
            }
        }
    else:
        assert 'center' not in relationships
    if 'om_one_one_local' in fields:
        assert relationships['om_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_45}'
                         '/relationships/om_one_one_local'),
                'related': f'/resources/one_manys/{UUID_45}/om_one_one_local',
            }
        }
    else:
        assert 'om_one_one_local' not in relationships
    if 'om_one_one_remote' in fields:
        assert relationships['om_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_45}'
                         '/relationships/om_one_one_remote'),
                'related': f'/resources/one_manys/{UUID_45}/om_one_one_remote',
            }
        }
    else:
        assert 'om_one_one_remote' not in relationships
    if 'om_many_one' in fields:
        assert relationships['om_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/one_manys/{UUID_45}'
                         '/relationships/om_many_one'),
                'related': f'/resources/one_manys/{UUID_45}/om_many_one',
            }
        }
    else:
        assert 'om_many_one' not in relationships
    if 'om_one_manys' in fields:
        assert relationships['om_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_manys/{UUID_45}'
                         '/relationships/om_one_manys'),
                'related': f'/resources/one_manys/{UUID_45}/om_one_manys',
            }
        }
    else:
        assert 'om_one_manys' not in relationships
    if 'om_many_manys' in fields:
        assert relationships['om_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/one_manys/{UUID_45}'
                         '/relationships/om_many_manys'),
                'related': f'/resources/one_manys/{UUID_45}/om_many_manys',
            }
        }
    else:
        assert 'om_many_manys' not in relationships


def assert_medium_model_included_object_many_manys_uuid_51(obj, fields=None):
    """Asserts that an included /many_manys/{UUID_51} object from the medium
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'mm_attr_int', 'mm_attr_str', 'centers', 'mm_one_one_local',
            'mm_one_one_remote', 'mm_many_one', 'mm_one_manys', 'mm_many_manys'
        }
    assert obj['type'] == 'many_manys'
    assert obj['id'] == UUID_51
    attributes = obj.get('attributes', {})
    if 'mm_attr_int' in fields:
        assert attributes['mm_attr_int'] == 881
    else:
        assert 'mm_attr_int' not in attributes
    if 'mm_attr_str' in fields:
        assert attributes['mm_attr_str'] == 'MM-one'
    else:
        assert 'mm_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'centers' in fields:
        assert relationships['centers'] == {
            'data': [{
                'type': 'centers',
                'id': UUID_1,
            }],
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/centers'),
                'related': f'/resources/many_manys/{UUID_51}/centers',
            }
        }
    else:
        assert 'centers' not in relationships
    if 'mm_one_one_local' in fields:
        assert relationships['mm_one_one_local'] == {
            'data': {
                'type': 'mm_one_one_locals',
                'id': UUID_511,
            },
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_one_one_local'),
                'related': f'/resources/many_manys/{UUID_51}/mm_one_one_local',
            }
        }
    else:
        assert 'mm_one_one_local' not in relationships
    if 'mm_one_one_remote' in fields:
        assert relationships['mm_one_one_remote'] == {
            'data': {
                'type': 'mm_one_one_remotes',
                'id': UUID_521,
            },
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_one_one_remote'),
                'related': f'/resources/many_manys/{UUID_51}/mm_one_one_remote',
            }
        }
    else:
        assert 'mm_one_one_remote' not in relationships
    if 'mm_many_one' in fields:
        assert relationships['mm_many_one'] == {
            'data': {
                'type': 'mm_many_ones',
                'id': UUID_531,
            },
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_many_one'),
                'related': f'/resources/many_manys/{UUID_51}/mm_many_one',
            }
        }
    else:
        assert 'mm_many_one' not in relationships
    if 'mm_one_manys' in fields:
        assert relationships['mm_one_manys'] == {
            'data': [
                {
                    'type': 'mm_one_manys',
                    'id': UUID_541,
                },
                {
                    'type': 'mm_one_manys',
                    'id': UUID_542,
                },
            ],
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_one_manys'),
                'related': f'/resources/many_manys/{UUID_51}/mm_one_manys',
            }
        }
    else:
        assert 'mm_one_manys' not in relationships
    if 'mm_many_manys' in fields:
        assert relationships['mm_many_manys'] == {
            'data': [
                {
                    'type': 'mm_many_manys',
                    'id': UUID_551,
                },
                {
                    'type': 'mm_many_manys',
                    'id': UUID_552,
                },
            ],
            'links': {
                'self': (f'/resources/many_manys/{UUID_51}'
                         '/relationships/mm_many_manys'),
                'related': f'/resources/many_manys/{UUID_51}/mm_many_manys',
            }
        }
    else:
        assert 'mm_many_manys' not in relationships


def assert_medium_model_included_object_many_manys_uuid_52(obj, fields=None):
    """Asserts that an included /many_manys/{UUID_52} object from the medium
    model is properly formatted.
    """
    assert_small_model_included_object_many_manys_uuid_52(obj, fields)


def assert_medium_model_included_object_many_manys_uuid_54(obj, fields=None):
    """Asserts that an included /many_manys/{UUID_54} object from the medium
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'mm_attr_int', 'mm_attr_str', 'centers', 'mm_one_one_local',
            'mm_one_one_remote', 'mm_many_one', 'mm_one_manys', 'mm_many_manys'
        }
    assert obj['type'] == 'many_manys'
    assert obj['id'] == UUID_54
    attributes = obj.get('attributes', {})
    if 'mm_attr_int' in fields:
        assert attributes['mm_attr_int'] == 884
    else:
        assert 'mm_attr_int' not in attributes
    if 'mm_attr_str' in fields:
        assert attributes['mm_attr_str'] == 'MM-four'
    else:
        assert 'mm_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'centers' in fields:
        assert relationships['centers'] == {
            'data': [{
                'type': 'centers',
                'id': UUID_2,
            }],
            'links': {
                'self': (f'/resources/many_manys/{UUID_54}'
                         '/relationships/centers'),
                'related': f'/resources/many_manys/{UUID_54}/centers',
            }
        }
    else:
        assert 'centers' not in relationships
    if 'mm_one_one_local' in fields:
        assert relationships['mm_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_54}'
                         '/relationships/mm_one_one_local'),
                'related': f'/resources/many_manys/{UUID_54}/mm_one_one_local',
            }
        }
    else:
        assert 'mm_one_one_local' not in relationships
    if 'mm_one_one_remote' in fields:
        assert relationships['mm_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_54}'
                         '/relationships/mm_one_one_remote'),
                'related': f'/resources/many_manys/{UUID_54}/mm_one_one_remote',
            }
        }
    else:
        assert 'mm_one_one_remote' not in relationships
    if 'mm_many_one' in fields:
        assert relationships['mm_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_54}'
                         '/relationships/mm_many_one'),
                'related': f'/resources/many_manys/{UUID_54}/mm_many_one',
            }
        }
    else:
        assert 'mm_many_one' not in relationships
    if 'mm_one_manys' in fields:
        assert relationships['mm_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_manys/{UUID_54}'
                         '/relationships/mm_one_manys'),
                'related': f'/resources/many_manys/{UUID_54}/mm_one_manys',
            }
        }
    else:
        assert 'mm_one_manys' not in relationships
    if 'mm_many_manys' in fields:
        assert relationships['mm_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_manys/{UUID_54}'
                         '/relationships/mm_many_manys'),
                'related': f'/resources/many_manys/{UUID_54}/mm_many_manys',
            }
        }
    else:
        assert 'mm_many_manys' not in relationships


def assert_medium_model_included_object_many_manys_uuid_55(obj, fields=None):
    """Asserts that an included /many_manys/{UUID_55} object from the medium
    model is properly formatted.
    """

    # pylint: disable=too-many-branches

    if fields is None:
        fields = {
            'mm_attr_int', 'mm_attr_str', 'centers', 'mm_one_one_local',
            'mm_one_one_remote', 'mm_many_one', 'mm_one_manys', 'mm_many_manys'
        }
    assert obj['type'] == 'many_manys'
    assert obj['id'] == UUID_55
    attributes = obj.get('attributes', {})
    if 'mm_attr_int' in fields:
        assert attributes['mm_attr_int'] == 885
    else:
        assert 'mm_attr_int' not in attributes
    if 'mm_attr_str' in fields:
        assert attributes['mm_attr_str'] == 'MM-five'
    else:
        assert 'mm_attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'centers' in fields:
        assert relationships['centers'] == {
            'data': [{
                'type': 'centers',
                'id': UUID_2,
            }],
            'links': {
                'self': (f'/resources/many_manys/{UUID_55}'
                         '/relationships/centers'),
                'related': f'/resources/many_manys/{UUID_55}/centers',
            }
        }
    else:
        assert 'centers' not in relationships
    if 'mm_one_one_local' in fields:
        assert relationships['mm_one_one_local'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_55}'
                         '/relationships/mm_one_one_local'),
                'related': f'/resources/many_manys/{UUID_55}/mm_one_one_local',
            }
        }
    else:
        assert 'mm_one_one_local' not in relationships
    if 'mm_one_one_remote' in fields:
        assert relationships['mm_one_one_remote'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_55}'
                         '/relationships/mm_one_one_remote'),
                'related': f'/resources/many_manys/{UUID_55}/mm_one_one_remote',
            }
        }
    else:
        assert 'mm_one_one_remote' not in relationships
    if 'mm_many_one' in fields:
        assert relationships['mm_many_one'] == {
            'data': None,
            'links': {
                'self': (f'/resources/many_manys/{UUID_55}'
                         '/relationships/mm_many_one'),
                'related': f'/resources/many_manys/{UUID_55}/mm_many_one',
            }
        }
    else:
        assert 'mm_many_one' not in relationships
    if 'mm_one_manys' in fields:
        assert relationships['mm_one_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_manys/{UUID_55}'
                         '/relationships/mm_one_manys'),
                'related': f'/resources/many_manys/{UUID_55}/mm_one_manys',
            }
        }
    else:
        assert 'mm_one_manys' not in relationships
    if 'mm_many_manys' in fields:
        assert relationships['mm_many_manys'] == {
            'data': [],
            'links': {
                'self': (f'/resources/many_manys/{UUID_55}'
                         '/relationships/mm_many_manys'),
                'related': f'/resources/many_manys/{UUID_55}/mm_many_manys',
            }
        }
    else:
        assert 'mm_many_manys' not in relationships


def assert_medium_model_included_object_ool_one_one_locals_uuid_111(
        obj, fields=None):
    """Asserts that an included /ool_one_one_locals/{UUID_111} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_local'}
    assert obj['type'] == 'ool_one_one_locals'
    assert obj['id'] == UUID_111
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 21211
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11L-11L-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_local' in fields:
        assert relationships['one_one_local'] == {
            'data': {
                'type': 'one_one_locals',
                'id': UUID_11,
            },
            'links': {
                'self': (f'/resources/ool_one_one_locals/{UUID_111}'
                         '/relationships/one_one_local'),
                'related': (f'/resources/ool_one_one_locals/{UUID_111}'
                            '/one_one_local'),
            }
        }
    else:
        assert 'one_one_local' not in relationships


def assert_medium_model_included_object_ool_one_one_remotes_uuid_121(
        obj, fields=None):
    """Asserts that an included /ool_one_one_remotes/{UUID_121} object from
    the medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_local'}
    assert obj['type'] == 'ool_one_one_remotes'
    assert obj['id'] == UUID_121
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 21121
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11L-11R-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_local' in fields:
        assert relationships['one_one_local'] == {
            'data': {
                'type': 'one_one_locals',
                'id': UUID_11,
            },
            'links': {
                'self': (f'/resources/ool_one_one_remotes/{UUID_121}'
                         '/relationships/one_one_local'),
                'related': (f'/resources/ool_one_one_remotes/{UUID_121}'
                            '/one_one_local'),
            }
        }
    else:
        assert 'one_one_local' not in relationships


def assert_medium_model_included_object_ool_many_ones_uuid_131(
        obj, fields=None):
    """Asserts that an included /ool_many_ones/{UUID_131} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_locals'}
    assert obj['type'] == 'ool_many_ones'
    assert obj['id'] == UUID_131
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 21811
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11L-M1-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_locals' in fields:
        assert relationships['one_one_locals'] == {
            'data': [{
                'type': 'one_one_locals',
                'id': UUID_11,
            }],
            'links': {
                'self': (f'/resources/ool_many_ones/{UUID_131}'
                         '/relationships/one_one_locals'),
                'related': (f'/resources/ool_many_ones/{UUID_131}'
                            '/one_one_locals'),
            }
        }
    else:
        assert 'one_one_locals' not in relationships


def assert_medium_model_included_object_ool_one_manys_uuid_141(
        obj, fields=None):
    """Asserts that an included /ool_one_manys/{UUID_141} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_local'}
    assert obj['type'] == 'ool_one_manys'
    assert obj['id'] == UUID_141
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 21181
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11L-1M-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_local' in fields:
        assert relationships['one_one_local'] == {
            'data': {
                'type': 'one_one_locals',
                'id': UUID_11,
            },
            'links': {
                'self': (f'/resources/ool_one_manys/{UUID_141}'
                         '/relationships/one_one_local'),
                'related': f'/resources/ool_one_manys/{UUID_141}/one_one_local',
            }
        }
    else:
        assert 'one_one_local' not in relationships


def assert_medium_model_included_object_ool_one_manys_uuid_142(
        obj, fields=None):
    """Asserts that an included /ool_one_manys/{UUID_142} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_local'}
    assert obj['type'] == 'ool_one_manys'
    assert obj['id'] == UUID_142
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 21182
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11L-1M-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_local' in fields:
        assert relationships['one_one_local'] == {
            'data': {
                'type': 'one_one_locals',
                'id': UUID_11,
            },
            'links': {
                'self': (f'/resources/ool_one_manys/{UUID_142}'
                         '/relationships/one_one_local'),
                'related': f'/resources/ool_one_manys/{UUID_142}/one_one_local',
            }
        }
    else:
        assert 'one_one_local' not in relationships


def assert_medium_model_included_object_ool_many_manys_uuid_151(
        obj, fields=None):
    """Asserts that an included /ool_many_manys/{UUID_151} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_locals'}
    assert obj['type'] == 'ool_many_manys'
    assert obj['id'] == UUID_151
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 21881
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11L-MM-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_locals' in fields:
        assert relationships['one_one_locals'] == {
            'data': [{
                'type': 'one_one_locals',
                'id': UUID_11,
            }],
            'links': {
                'self': (f'/resources/ool_many_manys/{UUID_151}'
                         '/relationships/one_one_locals'),
                'related': (f'/resources/ool_many_manys/{UUID_151}'
                            '/one_one_locals'),
            }
        }
    else:
        assert 'one_one_locals' not in relationships


def assert_medium_model_included_object_ool_many_manys_uuid_152(
        obj, fields=None):
    """Asserts that an included /ool_many_manys/{UUID_152} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_locals'}
    assert obj['type'] == 'ool_many_manys'
    assert obj['id'] == UUID_152
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 21882
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11L-MM-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_locals' in fields:
        assert relationships['one_one_locals'] == {
            'data': [{
                'type': 'one_one_locals',
                'id': UUID_11,
            }],
            'links': {
                'self': (f'/resources/ool_many_manys/{UUID_152}'
                         '/relationships/one_one_locals'),
                'related': (f'/resources/ool_many_manys/{UUID_152}'
                            '/one_one_locals'),
            }
        }
    else:
        assert 'one_one_locals' not in relationships


def assert_medium_model_included_object_oor_one_one_locals_uuid_211(
        obj, fields=None):
    """Asserts that an included /oor_one_one_locals/{UUID_211} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_remote'}
    assert obj['type'] == 'oor_one_one_locals'
    assert obj['id'] == UUID_211
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 12211
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11R-11L-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_remote' in fields:
        assert relationships['one_one_remote'] == {
            'data': {
                'type': 'one_one_remotes',
                'id': UUID_21,
            },
            'links': {
                'self': (f'/resources/oor_one_one_locals/{UUID_211}'
                         '/relationships/one_one_remote'),
                'related': (f'/resources/oor_one_one_locals/{UUID_211}'
                            '/one_one_remote'),
            }
        }
    else:
        assert 'one_one_remote' not in relationships


def assert_medium_model_included_object_oor_one_one_remotes_uuid_221(
        obj, fields=None):
    """Asserts that an included /oor_one_one_remotes/{UUID_221} object from
    the medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_remote'}
    assert obj['type'] == 'oor_one_one_remotes'
    assert obj['id'] == UUID_221
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 12121
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11R-11R-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_remote' in fields:
        assert relationships['one_one_remote'] == {
            'data': {
                'type': 'one_one_remotes',
                'id': UUID_21,
            },
            'links': {
                'self': (f'/resources/oor_one_one_remotes/{UUID_221}'
                         '/relationships/one_one_remote'),
                'related': (f'/resources/oor_one_one_remotes/{UUID_221}'
                            '/one_one_remote'),
            }
        }
    else:
        assert 'one_one_remote' not in relationships


def assert_medium_model_included_object_oor_many_ones_uuid_231(
        obj, fields=None):
    """Asserts that an included /oor_many_ones/{UUID_231} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_remotes'}
    assert obj['type'] == 'oor_many_ones'
    assert obj['id'] == UUID_231
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 12811
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11R-M1-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_remotes' in fields:
        assert relationships['one_one_remotes'] == {
            'data': [{
                'type': 'one_one_remotes',
                'id': UUID_21,
            }],
            'links': {
                'self': (f'/resources/oor_many_ones/{UUID_231}'
                         '/relationships/one_one_remotes'),
                'related': (f'/resources/oor_many_ones/{UUID_231}'
                            '/one_one_remotes'),
            }
        }
    else:
        assert 'one_one_remotes' not in relationships


def assert_medium_model_included_object_oor_one_manys_uuid_241(
        obj, fields=None):
    """Asserts that an included /oor_one_manys/{UUID_241} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_remote'}
    assert obj['type'] == 'oor_one_manys'
    assert obj['id'] == UUID_241
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 12181
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11R-1M-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_remote' in fields:
        assert relationships['one_one_remote'] == {
            'data': {
                'type': 'one_one_remotes',
                'id': UUID_21,
            },
            'links': {
                'self': (f'/resources/oor_one_manys/{UUID_241}'
                         '/relationships/one_one_remote'),
                'related': (f'/resources/oor_one_manys/{UUID_241}'
                            '/one_one_remote'),
            }
        }
    else:
        assert 'one_one_remote' not in relationships


def assert_medium_model_included_object_oor_one_manys_uuid_242(
        obj, fields=None):
    """Asserts that an included /oor_one_manys/{UUID_242} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_remote'}
    assert obj['type'] == 'oor_one_manys'
    assert obj['id'] == UUID_242
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 12182
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11R-1M-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_remote' in fields:
        assert relationships['one_one_remote'] == {
            'data': {
                'type': 'one_one_remotes',
                'id': UUID_21,
            },
            'links': {
                'self': (f'/resources/oor_one_manys/{UUID_242}'
                         '/relationships/one_one_remote'),
                'related': (f'/resources/oor_one_manys/{UUID_242}'
                            '/one_one_remote'),
            }
        }
    else:
        assert 'one_one_remote' not in relationships


def assert_medium_model_included_object_oor_many_manys_uuid_251(
        obj, fields=None):
    """Asserts that an included /oor_many_manys/{UUID_251} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_remotes'}
    assert obj['type'] == 'oor_many_manys'
    assert obj['id'] == UUID_251
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 12881
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11R-MM-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_remotes' in fields:
        assert relationships['one_one_remotes'] == {
            'data': [{
                'type': 'one_one_remotes',
                'id': UUID_21,
            }],
            'links': {
                'self': (f'/resources/oor_many_manys/{UUID_251}'
                         '/relationships/one_one_remotes'),
                'related': (f'/resources/oor_many_manys/{UUID_251}'
                            '/one_one_remotes'),
            }
        }
    else:
        assert 'one_one_remotes' not in relationships


def assert_medium_model_included_object_oor_many_manys_uuid_252(
        obj, fields=None):
    """Asserts that an included /oor_many_manys/{UUID_252} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_one_remotes'}
    assert obj['type'] == 'oor_many_manys'
    assert obj['id'] == UUID_252
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 12882
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '11R-MM-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_one_remotes' in fields:
        assert relationships['one_one_remotes'] == {
            'data': [{
                'type': 'one_one_remotes',
                'id': UUID_21,
            }],
            'links': {
                'self': (f'/resources/oor_many_manys/{UUID_252}'
                         '/relationships/one_one_remotes'),
                'related': (f'/resources/oor_many_manys/{UUID_252}'
                            '/one_one_remotes'),
            }
        }
    else:
        assert 'one_one_remotes' not in relationships


def assert_medium_model_included_object_mo_one_one_locals_uuid_311(
        obj, fields=None):
    """Asserts that an included /mo_one_one_locals/{UUID_311} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_one'}
    assert obj['type'] == 'mo_one_one_locals'
    assert obj['id'] == UUID_311
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 81211
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'M1-11L-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_one' in fields:
        assert relationships['many_one'] == {
            'data': {
                'type': 'many_ones',
                'id': UUID_31,
            },
            'links': {
                'self': (f'/resources/mo_one_one_locals/{UUID_311}'
                         '/relationships/many_one'),
                'related': f'/resources/mo_one_one_locals/{UUID_311}/many_one',
            }
        }
    else:
        assert 'many_one' not in relationships


def assert_medium_model_included_object_mo_one_one_remotes_uuid_321(
        obj, fields=None):
    """Asserts that an included /mo_one_one_remotes/{UUID_321} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_one'}
    assert obj['type'] == 'mo_one_one_remotes'
    assert obj['id'] == UUID_321
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 81121
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'M1-11R-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_one' in fields:
        assert relationships['many_one'] == {
            'data': {
                'type': 'many_ones',
                'id': UUID_31,
            },
            'links': {
                'self': (f'/resources/mo_one_one_remotes/{UUID_321}'
                         '/relationships/many_one'),
                'related': f'/resources/mo_one_one_remotes/{UUID_321}/many_one',
            }
        }
    else:
        assert 'many_one' not in relationships


def assert_medium_model_included_object_mo_many_ones_uuid_331(obj, fields=None):
    """Asserts that an included /mo_many_ones/{UUID_331} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_ones'}
    assert obj['type'] == 'mo_many_ones'
    assert obj['id'] == UUID_331
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 81811
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'M1-M1-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_ones' in fields:
        assert relationships['many_ones'] == {
            'data': [{
                'type': 'many_ones',
                'id': UUID_31,
            }],
            'links': {
                'self': (f'/resources/mo_many_ones/{UUID_331}'
                         '/relationships/many_ones'),
                'related': f'/resources/mo_many_ones/{UUID_331}/many_ones',
            }
        }
    else:
        assert 'many_ones' not in relationships


def assert_medium_model_included_object_mo_one_manys_uuid_341(obj, fields=None):
    """Asserts that an included /mo_one_manys/{UUID_341} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_one'}
    assert obj['type'] == 'mo_one_manys'
    assert obj['id'] == UUID_341
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 81181
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'M1-1M-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_one' in fields:
        assert relationships['many_one'] == {
            'data': {
                'type': 'many_ones',
                'id': UUID_31,
            },
            'links': {
                'self': (f'/resources/mo_one_manys/{UUID_341}'
                         '/relationships/many_one'),
                'related': f'/resources/mo_one_manys/{UUID_341}/many_one',
            }
        }
    else:
        assert 'many_one' not in relationships


def assert_medium_model_included_object_mo_one_manys_uuid_342(obj, fields=None):
    """Asserts that an included /mo_one_manys/{UUID_342} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_one'}
    assert obj['type'] == 'mo_one_manys'
    assert obj['id'] == UUID_342
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 81182
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'M1-1M-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_one' in fields:
        assert relationships['many_one'] == {
            'data': {
                'type': 'many_ones',
                'id': UUID_31,
            },
            'links': {
                'self': (f'/resources/mo_one_manys/{UUID_342}'
                         '/relationships/many_one'),
                'related': f'/resources/mo_one_manys/{UUID_342}/many_one',
            }
        }
    else:
        assert 'many_one' not in relationships


def assert_medium_model_included_object_mo_many_manys_uuid_351(
        obj, fields=None):
    """Asserts that an included /mo_many_manys/{UUID_351} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_ones'}
    assert obj['type'] == 'mo_many_manys'
    assert obj['id'] == UUID_351
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 81881
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'M1-MM-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_ones' in fields:
        assert relationships['many_ones'] == {
            'data': [{
                'type': 'many_ones',
                'id': UUID_31,
            }],
            'links': {
                'self': (f'/resources/mo_many_manys/{UUID_351}'
                         '/relationships/many_ones'),
                'related': f'/resources/mo_many_manys/{UUID_351}/many_ones',
            }
        }
    else:
        assert 'many_ones' not in relationships


def assert_medium_model_included_object_mo_many_manys_uuid_352(
        obj, fields=None):
    """Asserts that an included /mo_many_manys/{UUID_352} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_ones'}
    assert obj['type'] == 'mo_many_manys'
    assert obj['id'] == UUID_352
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 81882
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'M1-MM-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_ones' in fields:
        assert relationships['many_ones'] == {
            'data': [{
                'type': 'many_ones',
                'id': UUID_31,
            }],
            'links': {
                'self': (f'/resources/mo_many_manys/{UUID_352}'
                         '/relationships/many_ones'),
                'related': f'/resources/mo_many_manys/{UUID_352}/many_ones',
            }
        }
    else:
        assert 'many_ones' not in relationships


def assert_medium_model_included_object_om_one_one_locals_uuid_411(
        obj, fields=None):
    """Asserts that an included /om_one_one_locals/{UUID_411} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_many'}
    assert obj['type'] == 'om_one_one_locals'
    assert obj['id'] == UUID_411
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 18211
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '1M-11L-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_many' in fields:
        assert relationships['one_many'] == {
            'data': {
                'type': 'one_manys',
                'id': UUID_41,
            },
            'links': {
                'self': (f'/resources/om_one_one_locals/{UUID_411}'
                         '/relationships/one_many'),
                'related': f'/resources/om_one_one_locals/{UUID_411}/one_many',
            }
        }
    else:
        assert 'one_many' not in relationships


def assert_medium_model_included_object_om_one_one_remotes_uuid_421(
        obj, fields=None):
    """Asserts that an included /om_one_one_remotes/{UUID_421} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_many'}
    assert obj['type'] == 'om_one_one_remotes'
    assert obj['id'] == UUID_421
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 18121
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '1M-11R-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_many' in fields:
        assert relationships['one_many'] == {
            'data': {
                'type': 'one_manys',
                'id': UUID_41,
            },
            'links': {
                'self': (f'/resources/om_one_one_remotes/{UUID_421}'
                         '/relationships/one_many'),
                'related': f'/resources/om_one_one_remotes/{UUID_421}/one_many',
            }
        }
    else:
        assert 'one_many' not in relationships


def assert_medium_model_included_object_om_many_ones_uuid_431(obj, fields=None):
    """Asserts that an included /om_many_ones/{UUID_431} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_manys'}
    assert obj['type'] == 'om_many_ones'
    assert obj['id'] == UUID_431
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 18811
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '1M-M1-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_manys' in fields:
        assert relationships['one_manys'] == {
            'data': [{
                'type': 'one_manys',
                'id': UUID_41,
            }],
            'links': {
                'self': (f'/resources/om_many_ones/{UUID_431}'
                         '/relationships/one_manys'),
                'related': f'/resources/om_many_ones/{UUID_431}/one_manys',
            }
        }
    else:
        assert 'one_manys' not in relationships


def assert_medium_model_included_object_om_one_manys_uuid_441(obj, fields=None):
    """Asserts that an included /om_one_manys/{UUID_441} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_many'}
    assert obj['type'] == 'om_one_manys'
    assert obj['id'] == UUID_441
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 18181
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '1M-1M-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_many' in fields:
        assert relationships['one_many'] == {
            'data': {
                'type': 'one_manys',
                'id': UUID_41,
            },
            'links': {
                'self': (f'/resources/om_one_manys/{UUID_441}'
                         '/relationships/one_many'),
                'related': f'/resources/om_one_manys/{UUID_441}/one_many',
            }
        }
    else:
        assert 'one_many' not in relationships


def assert_medium_model_included_object_om_one_manys_uuid_442(obj, fields=None):
    """Asserts that an included /om_one_manys/{UUID_442} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_many'}
    assert obj['type'] == 'om_one_manys'
    assert obj['id'] == UUID_442
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 18182
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '1M-1M-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_many' in fields:
        assert relationships['one_many'] == {
            'data': {
                'type': 'one_manys',
                'id': UUID_41,
            },
            'links': {
                'self': (f'/resources/om_one_manys/{UUID_442}'
                         '/relationships/one_many'),
                'related': f'/resources/om_one_manys/{UUID_442}/one_many',
            }
        }
    else:
        assert 'one_many' not in relationships


def assert_medium_model_included_object_om_many_manys_uuid_451(
        obj, fields=None):
    """Asserts that an included /om_many_manys/{UUID_451} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_manys'}
    assert obj['type'] == 'om_many_manys'
    assert obj['id'] == UUID_451
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 18881
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '1M-MM-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_manys' in fields:
        assert relationships['one_manys'] == {
            'data': [{
                'type': 'one_manys',
                'id': UUID_41,
            }],
            'links': {
                'self': (f'/resources/om_many_manys/{UUID_451}'
                         '/relationships/one_manys'),
                'related': f'/resources/om_many_manys/{UUID_451}/one_manys',
            }
        }
    else:
        assert 'one_manys' not in relationships


def assert_medium_model_included_object_om_many_manys_uuid_452(
        obj, fields=None):
    """Asserts that an included /om_many_manys/{UUID_452} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'one_manys'}
    assert obj['type'] == 'om_many_manys'
    assert obj['id'] == UUID_452
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 18882
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == '1M-MM-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'one_manys' in fields:
        assert relationships['one_manys'] == {
            'data': [{
                'type': 'one_manys',
                'id': UUID_41,
            }],
            'links': {
                'self': (f'/resources/om_many_manys/{UUID_452}'
                         '/relationships/one_manys'),
                'related': f'/resources/om_many_manys/{UUID_452}/one_manys',
            }
        }
    else:
        assert 'one_manys' not in relationships


def assert_medium_model_included_object_mm_one_one_locals_uuid_511(
        obj, fields=None):
    """Asserts that an included /mm_one_one_locals/{UUID_511} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_many'}
    assert obj['type'] == 'mm_one_one_locals'
    assert obj['id'] == UUID_511
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 88211
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'MM-11L-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_many' in fields:
        assert relationships['many_many'] == {
            'data': {
                'type': 'many_manys',
                'id': UUID_51,
            },
            'links': {
                'self': (f'/resources/mm_one_one_locals/{UUID_511}'
                         '/relationships/many_many'),
                'related': f'/resources/mm_one_one_locals/{UUID_511}/many_many',
            }
        }
    else:
        assert 'many_many' not in relationships


def assert_medium_model_included_object_mm_one_one_remotes_uuid_521(
        obj, fields=None):
    """Asserts that an included /mm_one_one_remotes/{UUID_521} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_many'}
    assert obj['type'] == 'mm_one_one_remotes'
    assert obj['id'] == UUID_521
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 88121
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'MM-11R-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_many' in fields:
        assert relationships['many_many'] == {
            'data': {
                'type': 'many_manys',
                'id': UUID_51,
            },
            'links': {
                'self': (f'/resources/mm_one_one_remotes/{UUID_521}'
                         '/relationships/many_many'),
                'related': (f'/resources/mm_one_one_remotes/{UUID_521}'
                            '/many_many'),
            }
        }
    else:
        assert 'many_many' not in relationships


def assert_medium_model_included_object_mm_many_ones_uuid_531(obj, fields=None):
    """Asserts that an included /mm_many_ones/{UUID_531} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_manys'}
    assert obj['type'] == 'mm_many_ones'
    assert obj['id'] == UUID_531
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 88811
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'MM-M1-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_manys' in fields:
        assert relationships['many_manys'] == {
            'data': [{
                'type': 'many_manys',
                'id': UUID_51,
            }],
            'links': {
                'self': (f'/resources/mm_many_ones/{UUID_531}'
                         '/relationships/many_manys'),
                'related': f'/resources/mm_many_ones/{UUID_531}/many_manys',
            }
        }
    else:
        assert 'many_manys' not in relationships


def assert_medium_model_included_object_mm_one_manys_uuid_541(obj, fields=None):
    """Asserts that an included /mm_one_manys/{UUID_541} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_many'}
    assert obj['type'] == 'mm_one_manys'
    assert obj['id'] == UUID_541
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 88181
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'MM-1M-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_many' in fields:
        assert relationships['many_many'] == {
            'data': {
                'type': 'many_manys',
                'id': UUID_51,
            },
            'links': {
                'self': (f'/resources/mm_one_manys/{UUID_541}'
                         '/relationships/many_many'),
                'related': f'/resources/mm_one_manys/{UUID_541}/many_many',
            }
        }
    else:
        assert 'many_many' not in relationships


def assert_medium_model_included_object_mm_one_manys_uuid_542(obj, fields=None):
    """Asserts that an included /mm_one_manys/{UUID_542} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_many'}
    assert obj['type'] == 'mm_one_manys'
    assert obj['id'] == UUID_542
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 88182
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'MM-1M-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_many' in fields:
        assert relationships['many_many'] == {
            'data': {
                'type': 'many_manys',
                'id': UUID_51,
            },
            'links': {
                'self': (f'/resources/mm_one_manys/{UUID_542}'
                         '/relationships/many_many'),
                'related': f'/resources/mm_one_manys/{UUID_542}/many_many',
            }
        }
    else:
        assert 'many_many' not in relationships


def assert_medium_model_included_object_mm_many_manys_uuid_551(
        obj, fields=None):
    """Asserts that an included /mm_many_manys/{UUID_551} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_manys'}
    assert obj['type'] == 'mm_many_manys'
    assert obj['id'] == UUID_551
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 88881
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'MM-MM-one'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_manys' in fields:
        assert relationships['many_manys'] == {
            'data': [{
                'type': 'many_manys',
                'id': UUID_51,
            }],
            'links': {
                'self': (f'/resources/mm_many_manys/{UUID_551}'
                         '/relationships/many_manys'),
                'related': f'/resources/mm_many_manys/{UUID_551}/many_manys',
            }
        }
    else:
        assert 'many_manys' not in relationships


def assert_medium_model_included_object_mm_many_manys_uuid_552(
        obj, fields=None):
    """Asserts that an included /mm_many_manys/{UUID_552} object from the
    medium model is properly formatted.
    """

    if fields is None:
        fields = {'attr_int', 'attr_str', 'many_manys'}
    assert obj['type'] == 'mm_many_manys'
    assert obj['id'] == UUID_552
    attributes = obj.get('attributes', {})
    if 'attr_int' in fields:
        assert attributes['attr_int'] == 88882
    else:
        assert 'attr_int' not in attributes
    if 'attr_str' in fields:
        assert attributes['attr_str'] == 'MM-MM-two'
    else:
        assert 'attr_str' not in attributes
    relationships = obj.get('relationships', {})
    if 'many_manys' in fields:
        assert relationships['many_manys'] == {
            'data': [{
                'type': 'many_manys',
                'id': UUID_51,
            }],
            'links': {
                'self': (f'/resources/mm_many_manys/{UUID_552}'
                         '/relationships/many_manys'),
                'related': f'/resources/mm_many_manys/{UUID_552}/many_manys',
            }
        }
    else:
        assert 'many_manys' not in relationships


def assert_medium_model_included(included, content_by_type):
    """Asserts that all resource identification objects are represented
    correctly in included.
    """
    function_by_rio = {
        ('centers', UUID_1):
            assert_medium_model_included_object_centers_uuid_1,
        ('one_one_locals', UUID_11):
            assert_medium_model_included_object_one_one_locals_uuid_11,
        ('one_one_locals', UUID_14):
            assert_medium_model_included_object_one_one_locals_uuid_14,
        ('one_one_remotes', UUID_21):
            assert_medium_model_included_object_one_one_remotes_uuid_21,
        ('one_one_remotes', UUID_24):
            assert_medium_model_included_object_one_one_remotes_uuid_24,
        ('many_ones', UUID_31):
            assert_medium_model_included_object_many_ones_uuid_31,
        ('many_ones', UUID_34):
            assert_medium_model_included_object_many_ones_uuid_34,
        ('one_manys', UUID_41):
            assert_medium_model_included_object_one_manys_uuid_41,
        ('one_manys', UUID_42):
            assert_medium_model_included_object_one_manys_uuid_42,
        ('one_manys', UUID_44):
            assert_medium_model_included_object_one_manys_uuid_44,
        ('one_manys', UUID_45):
            assert_medium_model_included_object_one_manys_uuid_45,
        ('many_manys', UUID_51):
            assert_medium_model_included_object_many_manys_uuid_51,
        ('many_manys', UUID_52):
            assert_medium_model_included_object_many_manys_uuid_52,
        ('many_manys', UUID_54):
            assert_medium_model_included_object_many_manys_uuid_54,
        ('many_manys', UUID_55):
            assert_medium_model_included_object_many_manys_uuid_55,
        ('ool_one_one_locals', UUID_111):
            assert_medium_model_included_object_ool_one_one_locals_uuid_111,
        ('ool_one_one_remotes', UUID_121):
            assert_medium_model_included_object_ool_one_one_remotes_uuid_121,
        ('ool_many_ones', UUID_131):
            assert_medium_model_included_object_ool_many_ones_uuid_131,
        ('ool_one_manys', UUID_141):
            assert_medium_model_included_object_ool_one_manys_uuid_141,
        ('ool_one_manys', UUID_142):
            assert_medium_model_included_object_ool_one_manys_uuid_142,
        ('ool_many_manys', UUID_151):
            assert_medium_model_included_object_ool_many_manys_uuid_151,
        ('ool_many_manys', UUID_152):
            assert_medium_model_included_object_ool_many_manys_uuid_152,
        ('oor_one_one_locals', UUID_211):
            assert_medium_model_included_object_oor_one_one_locals_uuid_211,
        ('oor_one_one_remotes', UUID_221):
            assert_medium_model_included_object_oor_one_one_remotes_uuid_221,
        ('oor_many_ones', UUID_231):
            assert_medium_model_included_object_oor_many_ones_uuid_231,
        ('oor_one_manys', UUID_241):
            assert_medium_model_included_object_oor_one_manys_uuid_241,
        ('oor_one_manys', UUID_242):
            assert_medium_model_included_object_oor_one_manys_uuid_242,
        ('oor_many_manys', UUID_251):
            assert_medium_model_included_object_oor_many_manys_uuid_251,
        ('oor_many_manys', UUID_252):
            assert_medium_model_included_object_oor_many_manys_uuid_252,
        ('mo_one_one_locals', UUID_311):
            assert_medium_model_included_object_mo_one_one_locals_uuid_311,
        ('mo_one_one_remotes', UUID_321):
            assert_medium_model_included_object_mo_one_one_remotes_uuid_321,
        ('mo_many_ones', UUID_331):
            assert_medium_model_included_object_mo_many_ones_uuid_331,
        ('mo_one_manys', UUID_341):
            assert_medium_model_included_object_mo_one_manys_uuid_341,
        ('mo_one_manys', UUID_342):
            assert_medium_model_included_object_mo_one_manys_uuid_342,
        ('mo_many_manys', UUID_351):
            assert_medium_model_included_object_mo_many_manys_uuid_351,
        ('mo_many_manys', UUID_352):
            assert_medium_model_included_object_mo_many_manys_uuid_352,
        ('om_one_one_locals', UUID_411):
            assert_medium_model_included_object_om_one_one_locals_uuid_411,
        ('om_one_one_remotes', UUID_421):
            assert_medium_model_included_object_om_one_one_remotes_uuid_421,
        ('om_many_ones', UUID_431):
            assert_medium_model_included_object_om_many_ones_uuid_431,
        ('om_one_manys', UUID_441):
            assert_medium_model_included_object_om_one_manys_uuid_441,
        ('om_one_manys', UUID_442):
            assert_medium_model_included_object_om_one_manys_uuid_442,
        ('om_many_manys', UUID_451):
            assert_medium_model_included_object_om_many_manys_uuid_451,
        ('om_many_manys', UUID_452):
            assert_medium_model_included_object_om_many_manys_uuid_452,
        ('mm_one_one_locals', UUID_511):
            assert_medium_model_included_object_mm_one_one_locals_uuid_511,
        ('mm_one_one_remotes', UUID_521):
            assert_medium_model_included_object_mm_one_one_remotes_uuid_521,
        ('mm_many_ones', UUID_531):
            assert_medium_model_included_object_mm_many_ones_uuid_531,
        ('mm_one_manys', UUID_541):
            assert_medium_model_included_object_mm_one_manys_uuid_541,
        ('mm_one_manys', UUID_542):
            assert_medium_model_included_object_mm_one_manys_uuid_542,
        ('mm_many_manys', UUID_551):
            assert_medium_model_included_object_mm_many_manys_uuid_551,
        ('mm_many_manys', UUID_552):
            assert_medium_model_included_object_mm_many_manys_uuid_552,
    }

    obj_by_rio = {(obj['type'], obj['id']): obj for obj in included}
    rios = {(obj_type, obj_id)
            for obj_type, content in content_by_type.items()
            for obj_id in content[0]}
    assert obj_by_rio.keys() == rios
    fields = {
        obj_type: content[1] for obj_type, content in content_by_type.items()
    }
    for rio, obj in obj_by_rio.items():
        function_by_rio[rio](obj, fields[rio[0]])
