# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts.patch_relationships provides functions to test the PATCH
/{collection}/{id}/relationships/{relationship} responses from the JSON API
application.
"""

from ajsonapi.functests.asserts.generic import assert_errors, assert_no_content


#
# Successful requests/responses
#
async def assert_patch_relationship(response):
    """Verifies the response to a successful PATCH
    /{collection}/{id}/relationships/{relationship} request.
    """

    await assert_no_content(response)


#
# Failed requests/responses
#
async def assert_patch_relationship_conflict(response):
    """Verifies the response to a failed PATCH
    /{collection}/{id}/relationships/{relationship} request where the request
    document contains a resource identifier object that is already part of
    another one-to-one relationship already.
    """
    assert response.status == 409
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '409'
    assert error['title'] == 'Conflicting resource identifier object.'
    assert error['detail'] == ('Resource identifier object is part of another '
                               'one-to-one relationship.')
    assert error['source']['pointer'] == '/data'
    return document
