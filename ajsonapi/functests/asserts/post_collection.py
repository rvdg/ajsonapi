# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts.post_collection provides functions to test the POST
/{collection} responses from the JSON API application.
"""

from ajsonapi.functests.asserts.generic import (
    assert_data,
    assert_errors,
    assert_no_content,
    assert_nonexistent,
)


#
# Successful requests/responses
#
async def assert_post_collection_without_id(response):
    """Verifies the response to a successful POST /{collection} request where
    the request's document's data member does not contain a uuid.
    """

    assert response.status == 201
    assert response.headers['Content-Type'] == 'application/vnd.api+json'
    document = await response.json()
    assert isinstance(document, dict)
    assert 'data' in document

    data = document['data']
    assert isinstance(data, dict)
    assert 'type' in data
    type_name = data['type']
    assert type_name == response.url.path.split('/')[-1]
    assert 'id' in data
    assert 'links' in data
    links = data['links']
    assert isinstance(links, dict)
    assert 'self' in links
    assert links['self'] == response.headers['Location']

    assert set(data.keys()) <= set(
        ['type', 'id', 'attributes', 'relationships', 'links'])
    return document


async def assert_post_collection_with_id(response):
    """Verifies the response to a successful POST /{collection} request where
    the request's document's data member contains a uuid.
    """

    await assert_no_content(response)


#
# Failed requests/responses
#
async def assert_post_collection_data_duplicate_id(response, pointer):
    """Verifies the response for a failed POST /{collection} request where the
    request's document's data member contains a resource object with an id
    that already exists.
    """

    assert response.status == 409
    document = await assert_errors(response)
    errors = document['errors']
    assert len(errors) == 1
    error = errors[0]
    assert error['status'] == '409'
    assert error['title'] == 'Invalid resource object.'
    assert error['detail'] == 'Id already exists.'
    assert error['source']['pointer'] == pointer


async def assert_post_collection_nonexistent_collection(response, path):
    """Verifies the response to a failed POST /{collection} request where
    {collection} does not exist.
    """

    return await assert_nonexistent(response, path)


async def assert_post_collection_data_malformed_uuid(response, pointer):
    """Verifies the response for a failed POST /{collection} request where the
    request's document's data member contains a resource object with a
    malformed UUID.
    """

    return await assert_data(response,
                             'Invalid resource object.',
                             'Malformed client-generated id.',
                             pointers={pointer})
