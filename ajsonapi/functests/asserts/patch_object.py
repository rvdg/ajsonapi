# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module asserts.patch_object provides functions to test the PATCH
/{collection}/{id} responses from the JSON API application.
"""

from ajsonapi.functests.asserts.generic import (
    assert_data_invalid,
    assert_object,
)
from ajsonapi.functests.asserts.model import assert_centers_uuid_1_related


#
# Successful requests/responses
#
async def assert_patch_object(response):
    """Verifies a response to a successful PATCH /{collection}/{id} request.
    """

    return await assert_object(response)


async def assert_patch_centers_uuid_1_related(response):
    """Verifies the response to a successful PATCH /centers/{UUID_1} request
    where the resulting /centers/{UUID_1} object has all its functional test
    relationships in place.
    """

    document = await assert_object(response)
    data = document['data']
    assert_centers_uuid_1_related(data)
    return document


#
# Failed reqeusts/responses
#
async def assert_patch_object_data_invalid_id(response, pointer):
    """Verifies a response to a failed PATCH /{collection}/{id} request where
    the request document's data member contains a resource object with an
    invalid id.
    """

    return await assert_data_invalid(response,
                                     detail='Invalid id.',
                                     pointers={pointer})
