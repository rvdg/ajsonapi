# Copyright © 2019 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Module xxx provides xxx."""

import pytest
from asserts.post_collection import assert_post_collection_without_id

from ajsonapi import JSON_API, Attribute, String
from ajsonapi.functests.headers import SEND_HEADERS


class Users(JSON_API):
    """Users contains a name attribute that used to be overridden by ajsonapi.
    """
    # pylint: disable=too-few-public-methods
    name = Attribute(String)


@pytest.mark.asyncio
async def test_name_attribute(client):
    """Functional test for a POST /resources/{collection} request where the data
    contains the name attribute.
    """
    url = '/resources/users'
    json = {'data': {'type': 'users', 'attributes': {'name': 'Roel'}}}
    async with client.post(url, headers=SEND_HEADERS, json=json) as response:
        document = await assert_post_collection_without_id(response)
        user = document['data']
        assert user['attributes']['name'] == 'Roel'
