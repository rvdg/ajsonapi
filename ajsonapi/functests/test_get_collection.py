# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Functional tests for GET /resources/{collection}"""

# pylint: disable=too-many-lines

import pytest
from multidict import MultiDict

import ajsonapi.functests.model  # pylint: disable=unused-import
from ajsonapi.functests.asserts.generic import (
    assert_accept_parameters,
    assert_content_type_parameter,
    assert_duplicate_parameters,
    assert_fields_invalid_field,
    assert_fields_invalid_resource,
    assert_filter_invalid_path,
    assert_include_invalid_path,
    assert_sort_invalid_field,
)
from ajsonapi.functests.asserts.get_collection import (
    assert_get_collection,
    assert_get_collection_nonexistent_collection,
)
from ajsonapi.functests.asserts.model import (
    assert_centers_id_unrelated,
    assert_centers_uuid_1_related,
    assert_centers_uuid_2_unrelated,
    assert_centers_uuid_3_unrelated,
)
from ajsonapi.functests.asserts.model_included import (
    assert_medium_model_included,
    assert_small_model_included,
)
from ajsonapi.functests.headers import HEADERS
from ajsonapi.functests.model_init import model_extend, model_init
from ajsonapi.functests.model_objects import (
    UUID_1,
    UUID_2,
    UUID_3,
    UUID_11,
    UUID_12,
    UUID_14,
    UUID_21,
    UUID_22,
    UUID_24,
    UUID_31,
    UUID_32,
    UUID_34,
    UUID_41,
    UUID_42,
    UUID_44,
    UUID_45,
    UUID_51,
    UUID_52,
    UUID_54,
    UUID_55,
    UUID_111,
    UUID_121,
    UUID_131,
    UUID_141,
    UUID_142,
    UUID_151,
    UUID_152,
    UUID_211,
    UUID_221,
    UUID_231,
    UUID_241,
    UUID_242,
    UUID_251,
    UUID_252,
    UUID_311,
    UUID_321,
    UUID_331,
    UUID_341,
    UUID_342,
    UUID_351,
    UUID_352,
    UUID_411,
    UUID_421,
    UUID_431,
    UUID_441,
    UUID_442,
    UUID_451,
    UUID_452,
    UUID_511,
    UUID_521,
    UUID_531,
    UUID_541,
    UUID_542,
    UUID_551,
    UUID_552,
)
from ajsonapi.functests.posts import post_centers


#
# Successful requests
#
@pytest.mark.asyncio
async def test_get_collection(client):
    """Functional tests for a successful GET /resources/{collection} request."""

    url = '/resources/centers'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    await model_init(client)

    url = '/resources/centers'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 3
        assert {obj['id'] for obj in data} == {UUID_1, UUID_2, UUID_3}
        for center in data:
            if center['id'] == UUID_1:
                assert_centers_uuid_1_related(center)
            elif center['id'] == UUID_2:
                assert_centers_uuid_2_unrelated(center)
            else:  # center['id'] == UUID_3
                assert_centers_uuid_3_unrelated(center)
        assert 'included' not in document

    document = await post_centers(client)
    center_id = document['data']['id']

    url = '/resources/centers'
    async with client.get(url, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 4
        assert {obj['id'] for obj in data} == \
                {UUID_1, UUID_2, UUID_3, center_id}
        for center in data:
            if center['id'] == UUID_1:
                assert_centers_uuid_1_related(center)
            elif center['id'] == UUID_2:
                assert_centers_uuid_2_unrelated(center)
            elif center['id'] == UUID_3:
                assert_centers_uuid_3_unrelated(center)
            else:
                assert_centers_id_unrelated(center, center_id)
        assert 'included' not in document


@pytest.mark.asyncio
async def test_get_collection_query_include(client):
    """Functional test for a successful GET /resources/{collection}?include=x
    request.
    """

    # pylint: disable=too-many-statements

    await model_init(client)

    url = '/resources/centers'
    params = {'include': 'one_one_local'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'one_one_locals': ({UUID_11}, None)})

    url = '/resources/centers'
    params = {'include': 'one_one_remote'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'one_one_remotes': ({UUID_21}, None)})

    url = '/resources/centers'
    params = {'include': 'many_one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'many_ones': ({UUID_31}, None)})

    url = '/resources/centers'
    params = {'include': 'one_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'one_manys': ({UUID_41, UUID_42}, None)})

    url = '/resources/centers'
    params = {'include': 'many_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'many_manys': ({UUID_51, UUID_52}, None)})

    await model_extend(client)

    url = '/resources/centers'
    params = {'include': 'one_one_local.ool_one_one_local'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, None),
                'ool_one_one_locals': ({UUID_111}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_local.ool_one_one_remote'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, None),
                'ool_one_one_remotes': ({UUID_121}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_local.ool_many_one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, None),
                'ool_many_ones': ({UUID_131}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_local.ool_one_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, None),
                'ool_one_manys': ({UUID_141, UUID_142}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_local.ool_many_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, None),
                'ool_many_manys': ({UUID_151, UUID_152}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_remote.oor_one_one_local'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, None),
                'oor_one_one_locals': ({UUID_211}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_remote.oor_one_one_remote'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, None),
                'oor_one_one_remotes': ({UUID_221}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_remote.oor_many_one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, None),
                'oor_many_ones': ({UUID_231}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_remote.oor_one_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, None),
                'oor_one_manys': ({UUID_241, UUID_242}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_one_remote.oor_many_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, None),
                'oor_many_manys': ({UUID_251, UUID_252}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_one.mo_one_one_local'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, None),
                'mo_one_one_locals': ({UUID_311}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_one.mo_one_one_remote'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, None),
                'mo_one_one_remotes': ({UUID_321}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_one.mo_many_one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, None),
                'mo_many_ones': ({UUID_331}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_one.mo_one_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, None),
                'mo_one_manys': ({UUID_341, UUID_342}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_one.mo_many_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, None),
                'mo_many_manys': ({UUID_351, UUID_352}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_manys.om_one_one_local'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, None),
                'om_one_one_locals': ({UUID_411}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_manys.om_one_one_remote'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, None),
                'om_one_one_remotes': ({UUID_421}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_manys.om_many_one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, None),
                'om_many_ones': ({UUID_431}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_manys.om_one_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, None),
                'om_one_manys': ({UUID_441, UUID_442}, None)
            })

    url = '/resources/centers'
    params = {'include': 'one_manys.om_many_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, None),
                'om_many_manys': ({UUID_451, UUID_452}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_manys.mm_one_one_local'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, None),
                'mm_one_one_locals': ({UUID_511}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_manys.mm_one_one_remote'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, None),
                'mm_one_one_remotes': ({UUID_521}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_manys.mm_many_one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, None),
                'mm_many_ones': ({UUID_531}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_manys.mm_one_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, None),
                'mm_one_manys': ({UUID_541, UUID_542}, None)
            })

    url = '/resources/centers'
    params = {'include': 'many_manys.mm_many_manys'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, None),
                'mm_many_manys': ({UUID_551, UUID_552}, None)
            })


@pytest.mark.asyncio
async def test_get_collection_query_fields(client):
    """Functional test for a successful GET /resources/{collection}?fields[x]=x
    request.
    """

    # pylint: disable=too-many-statements

    await model_init(client)

    url = '/resources/centers'
    params = {'fields[centers]': ''}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 3
        assert {obj['id'] for obj in data} == {UUID_1, UUID_2, UUID_3}
        for center in data:
            assert set(center.keys()) == {'type', 'id'}
            assert center['type'] == 'centers'
        assert 'included' not in document
    url = '/resources/centers'
    params = {'fields[centers]': 'attr_int,attr_str'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 3
        assert {obj['id'] for obj in data} == {UUID_1, UUID_2, UUID_3}
        for center in data:
            assert set(center.keys()) == {'type', 'id', 'attributes'}
            assert center['type'] == 'centers'
            assert set(center['attributes'].keys()) == {'attr_int', 'attr_str'}
        assert 'included' not in document
    url = '/resources/centers'
    params = {
        'fields[centers]':
            'one_one_local,one_one_remote,many_one,one_manys,many_manys'
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 3
        assert {obj['id'] for obj in data} == {UUID_1, UUID_2, UUID_3}
        for center in data:
            assert set(center.keys()) == {'type', 'id', 'relationships'}
            assert center['type'] == 'centers'
            assert set(center['relationships'].keys()) == {
                'one_one_local', 'one_one_remote', 'many_one', 'one_manys',
                'many_manys'
            }
        assert 'included' not in document
    url = '/resources/centers'
    params = {'fields[centers]': 'attr_int,one_one_local,one_one_remote'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 3
        assert {obj['id'] for obj in data} == {UUID_1, UUID_2, UUID_3}
        for center in data:
            assert set(
                center.keys()) == {'type', 'id', 'attributes', 'relationships'}
            assert center['type'] == 'centers'
            assert set(center['attributes'].keys()) == {'attr_int'}
            assert set(center['relationships'].keys()) == {
                'one_one_local', 'one_one_remote'
            }
        assert 'included' not in document


@pytest.mark.asyncio
async def test_get_collection_query_include_fields(client):
    """Functional test for a successful GET
    /resources/{collection}?include=x,fields[x]=x request.
    """

    # pylint: disable=too-many-statements

    await model_init(client)

    url = '/resources/centers'
    params = {
        'include': 'one_one_local',
        'fields[one_one_locals]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'one_one_locals': ({UUID_11}, set())})
    url = '/resources/centers'
    params = {
        'include': 'one_one_local',
        'fields[one_one_locals]': 'ool_attr_int,ool_one_one_local',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'], {
            'one_one_locals': ({UUID_11}, {'ool_attr_int', 'ool_one_one_local'})
        })

    url = '/resources/centers'
    params = {
        'include': 'one_one_remote',
        'fields[one_one_remotes]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'one_one_remotes': ({UUID_21}, set())})
    url = '/resources/centers'
    params = {
        'include': 'one_one_remote',
        'fields[one_one_remotes]': 'oor_attr_int,oor_one_one_remote',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'], {
            'one_one_remotes':
                ({UUID_21}, {'oor_attr_int', 'oor_one_one_remote'})
        })

    url = '/resources/centers'
    params = {
        'include': 'many_one',
        'fields[many_ones]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'many_ones': ({UUID_31}, set())})
    url = '/resources/centers'
    params = {
        'include': 'many_one',
        'fields[many_ones]': 'mo_attr_int,mo_many_one',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(
            document['included'],
            {'many_ones': ({UUID_31}, {'mo_attr_int', 'mo_many_one'})})

    url = '/resources/centers'
    params = {
        'include': 'one_manys',
        'fields[one_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'one_manys': ({UUID_41, UUID_42}, set())})
    url = '/resources/centers'
    params = {
        'include': 'one_manys',
        'fields[one_manys]': 'om_attr_int,om_one_manys',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'], {
            'one_manys': ({UUID_41, UUID_42}, {'om_attr_int', 'om_one_manys'})
        })

    url = '/resources/centers'
    params = {
        'include': 'many_manys',
        'fields[many_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'],
                                    {'many_manys': ({UUID_51, UUID_52}, set())})
    url = '/resources/centers'
    params = {
        'include': 'many_manys',
        'fields[many_manys]': 'mm_attr_int,mm_many_manys',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_small_model_included(document['included'], {
            'many_manys': ({UUID_51, UUID_52}, {'mm_attr_int', 'mm_many_manys'})
        })

    await model_extend(client)

    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_one_one_local',
        'fields[one_one_locals]': '',
        'fields[ool_one_one_locals]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, set()),
                'ool_one_one_locals': ({UUID_111}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_one_one_local',
        'fields[one_one_locals]': 'ool_attr_int,ool_one_one_local',
        'fields[ool_one_one_locals]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals':
                    ({UUID_11, UUID_14}, {'ool_attr_int', 'ool_one_one_local'}),
                'ool_one_one_locals': ({UUID_111}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_one_one_remote',
        'fields[one_one_locals]': '',
        'fields[ool_one_one_remotes]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, set()),
                'ool_one_one_remotes': ({UUID_121}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_one_one_remote',
        'fields[one_one_locals]': 'ool_attr_int,ool_one_one_remote',
        'fields[ool_one_one_remotes]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14
                                   }, {'ool_attr_int', 'ool_one_one_remote'}),
                'ool_one_one_remotes': ({UUID_121}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_many_one',
        'fields[one_one_locals]': '',
        'fields[ool_many_ones]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, set()),
                'ool_many_ones': ({UUID_131}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_many_one',
        'fields[one_one_locals]': 'ool_attr_int,ool_many_one',
        'fields[ool_many_ones]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals':
                    ({UUID_11, UUID_14}, {'ool_attr_int', 'ool_many_one'}),
                'ool_many_ones': ({UUID_131}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_one_manys',
        'fields[one_one_locals]': '',
        'fields[ool_one_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, set()),
                'ool_one_manys': ({UUID_141, UUID_142}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_one_manys',
        'fields[one_one_locals]': 'ool_attr_int,ool_one_manys',
        'fields[ool_one_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals':
                    ({UUID_11, UUID_14}, {'ool_attr_int', 'ool_one_manys'}),
                'ool_one_manys': ({UUID_141, UUID_142}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_many_manys',
        'fields[one_one_locals]': '',
        'fields[ool_many_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals': ({UUID_11, UUID_14}, set()),
                'ool_many_manys': ({UUID_151, UUID_152}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_local.ool_many_manys',
        'fields[one_one_locals]': 'ool_attr_int,ool_many_manys',
        'fields[ool_many_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_locals':
                    ({UUID_11, UUID_14}, {'ool_attr_int', 'ool_many_manys'}),
                'ool_many_manys': ({UUID_151, UUID_152}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_one_one_local',
        'fields[one_one_remotes]': '',
        'fields[oor_one_one_locals]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, set()),
                'oor_one_one_locals': ({UUID_211}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_one_one_local',
        'fields[one_one_remotes]': 'oor_attr_int,oor_one_one_local',
        'fields[oor_one_one_locals]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes':
                    ({UUID_21, UUID_24}, {'oor_attr_int', 'oor_one_one_local'}),
                'oor_one_one_locals': ({UUID_211}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_one_one_remote',
        'fields[one_one_remotes]': '',
        'fields[oor_one_one_remotes]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, set()),
                'oor_one_one_remotes': ({UUID_221}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_one_one_remote',
        'fields[one_one_remotes]': 'oor_attr_int,oor_one_one_remote',
        'fields[oor_one_one_remotes]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24
                                    }, {'oor_attr_int', 'oor_one_one_remote'}),
                'oor_one_one_remotes': ({UUID_221}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_many_one',
        'fields[one_one_remotes]': '',
        'fields[oor_many_ones]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, set()),
                'oor_many_ones': ({UUID_231}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_many_one',
        'fields[one_one_remotes]': 'oor_attr_int,oor_many_one',
        'fields[oor_many_ones]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes':
                    ({UUID_21, UUID_24}, {'oor_attr_int', 'oor_many_one'}),
                'oor_many_ones': ({UUID_231}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_one_manys',
        'fields[one_one_remotes]': '',
        'fields[oor_one_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, set()),
                'oor_one_manys': ({UUID_241, UUID_242}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_one_manys',
        'fields[one_one_remotes]': 'oor_attr_int,oor_one_manys',
        'fields[oor_one_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes':
                    ({UUID_21, UUID_24}, {'oor_attr_int', 'oor_one_manys'}),
                'oor_one_manys': ({UUID_241, UUID_242}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_many_manys',
        'fields[one_one_remotes]': '',
        'fields[oor_many_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes': ({UUID_21, UUID_24}, set()),
                'oor_many_manys': ({UUID_251, UUID_252}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_one_remote.oor_many_manys',
        'fields[one_one_remotes]': 'oor_attr_int,oor_many_manys',
        'fields[oor_many_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_one_remotes':
                    ({UUID_21, UUID_24}, {'oor_attr_int', 'oor_many_manys'}),
                'oor_many_manys': ({UUID_251, UUID_252}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_one_one_local',
        'fields[many_ones]': '',
        'fields[mo_one_one_locals]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, set()),
                'mo_one_one_locals': ({UUID_311}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_one_one_local',
        'fields[many_ones]': 'mo_attr_int,mo_one_one_local',
        'fields[mo_one_one_locals]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones':
                    ({UUID_31, UUID_34}, {'mo_attr_int', 'mo_one_one_local'}),
                'mo_one_one_locals': ({UUID_311}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_one_one_remote',
        'fields[many_ones]': '',
        'fields[mo_one_one_remotes]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, set()),
                'mo_one_one_remotes': ({UUID_321}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_one_one_remote',
        'fields[many_ones]': 'mo_attr_int,mo_one_one_remote',
        'fields[mo_one_one_remotes]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones':
                    ({UUID_31, UUID_34}, {'mo_attr_int', 'mo_one_one_remote'}),
                'mo_one_one_remotes': ({UUID_321}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_many_one',
        'fields[many_ones]': '',
        'fields[mo_many_ones]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, set()),
                'mo_many_ones': ({UUID_331}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_many_one',
        'fields[many_ones]': 'mo_attr_int,mo_many_one',
        'fields[mo_many_ones]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones':
                    ({UUID_31, UUID_34}, {'mo_attr_int', 'mo_many_one'}),
                'mo_many_ones': ({UUID_331}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_one_manys',
        'fields[many_ones]': '',
        'fields[mo_one_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, set()),
                'mo_one_manys': ({UUID_341, UUID_342}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_one_manys',
        'fields[many_ones]': 'mo_attr_int,mo_one_manys',
        'fields[mo_one_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones':
                    ({UUID_31, UUID_34}, {'mo_attr_int', 'mo_one_manys'}),
                'mo_one_manys': ({UUID_341, UUID_342}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_many_manys',
        'fields[many_ones]': '',
        'fields[mo_many_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones': ({UUID_31, UUID_34}, set()),
                'mo_many_manys': ({UUID_351, UUID_352}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_one.mo_many_manys',
        'fields[many_ones]': 'mo_attr_int,mo_many_manys',
        'fields[mo_many_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_ones':
                    ({UUID_31, UUID_34}, {'mo_attr_int', 'mo_many_manys'}),
                'mo_many_manys': ({UUID_351, UUID_352}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_one_one_local',
        'fields[one_manys]': '',
        'fields[om_one_one_locals]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, set()),
                'om_one_one_locals': ({UUID_411}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_one_one_local',
        'fields[one_manys]': 'om_attr_int,om_one_one_local',
        'fields[om_one_one_locals]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45
                              }, {'om_attr_int', 'om_one_one_local'}),
                'om_one_one_locals': ({UUID_411}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_one_one_remote',
        'fields[one_manys]': '',
        'fields[om_one_one_remotes]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, set()),
                'om_one_one_remotes': ({UUID_421}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_one_one_remote',
        'fields[one_manys]': 'om_attr_int,om_one_one_remote',
        'fields[om_one_one_remotes]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45
                              }, {'om_attr_int', 'om_one_one_remote'}),
                'om_one_one_remotes': ({UUID_421}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_many_one',
        'fields[one_manys]': '',
        'fields[om_many_ones]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, set()),
                'om_many_ones': ({UUID_431}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_many_one',
        'fields[one_manys]': 'om_attr_int,om_many_one',
        'fields[om_many_ones]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45
                              }, {'om_attr_int', 'om_many_one'}),
                'om_many_ones': ({UUID_431}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_one_manys',
        'fields[one_manys]': '',
        'fields[om_one_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, set()),
                'om_one_manys': ({UUID_441, UUID_442}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_one_manys',
        'fields[one_manys]': 'om_attr_int,om_one_manys',
        'fields[om_one_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45
                              }, {'om_attr_int', 'om_one_manys'}),
                'om_one_manys': ({UUID_441, UUID_442}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_many_manys',
        'fields[one_manys]': '',
        'fields[om_many_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45}, set()),
                'om_many_manys': ({UUID_451, UUID_452}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'one_manys.om_many_manys',
        'fields[one_manys]': 'om_attr_int,om_many_manys',
        'fields[om_many_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'one_manys': ({UUID_41, UUID_42, UUID_44, UUID_45
                              }, {'om_attr_int', 'om_many_manys'}),
                'om_many_manys': ({UUID_451, UUID_452}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_one_one_local',
        'fields[many_manys]': '',
        'fields[mm_one_one_locals]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, set()),
                'mm_one_one_locals': ({UUID_511}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_one_one_local',
        'fields[many_manys]': 'mm_attr_int,mm_one_one_local',
        'fields[mm_one_one_locals]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55
                               }, {'mm_attr_int', 'mm_one_one_local'}),
                'mm_one_one_locals': ({UUID_511}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_one_one_remote',
        'fields[many_manys]': '',
        'fields[mm_one_one_remotes]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, set()),
                'mm_one_one_remotes': ({UUID_521}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_one_one_remote',
        'fields[many_manys]': 'mm_attr_int,mm_one_one_remote',
        'fields[mm_one_one_remotes]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55
                               }, {'mm_attr_int', 'mm_one_one_remote'}),
                'mm_one_one_remotes': ({UUID_521}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_many_one',
        'fields[many_manys]': '',
        'fields[mm_many_ones]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, set()),
                'mm_many_ones': ({UUID_531}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_many_one',
        'fields[many_manys]': 'mm_attr_int,mm_many_one',
        'fields[mm_many_ones]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55
                               }, {'mm_attr_int', 'mm_many_one'}),
                'mm_many_ones': ({UUID_531}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_one_manys',
        'fields[many_manys]': '',
        'fields[mm_one_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, set()),
                'mm_one_manys': ({UUID_541, UUID_542}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_one_manys',
        'fields[many_manys]': 'mm_attr_int,mm_one_manys',
        'fields[mm_one_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55
                               }, {'mm_attr_int', 'mm_one_manys'}),
                'mm_one_manys': ({UUID_541, UUID_542}, {'attr_str'})
            })

    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_many_manys',
        'fields[many_manys]': '',
        'fields[mm_many_manys]': '',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55}, set()),
                'mm_many_manys': ({UUID_551, UUID_552}, set())
            })
    url = '/resources/centers'
    params = {
        'include': 'many_manys.mm_many_manys',
        'fields[many_manys]': 'mm_attr_int,mm_many_manys',
        'fields[mm_many_manys]': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        assert_medium_model_included(
            document['included'], {
                'many_manys': ({UUID_51, UUID_52, UUID_54, UUID_55
                               }, {'mm_attr_int', 'mm_many_manys'}),
                'mm_many_manys': ({UUID_551, UUID_552}, {'attr_str'})
            })


@pytest.mark.asyncio
async def test_get_collection_query_filter(client):
    """Functional test for a successful GET /resources/{collection}?filter[x]=x
    request.
    """

    # pylint: disable=too-many-statements

    await model_init(client)

    url = '/resources/centers'
    params = {'filter[attr_int]': '1'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_int]': '2'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_2_unrelated(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_int]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_int]': '1,2'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 2
        assert {obj['id'] for obj in data} == {UUID_1, UUID_2}
        for center in data:
            if center['id'] == UUID_1:
                assert_centers_uuid_1_related(center)
            else:  # center['id'] == UUID_2
                assert_centers_uuid_2_unrelated(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_str]': 'one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_str]': 'two'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_2_unrelated(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_str]': 'xxxxxxxx'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_bool]': 'true'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 2
        assert {obj['id'] for obj in data} == {UUID_2, UUID_3}
        for center in data:
            if center['id'] == UUID_2:
                assert_centers_uuid_2_unrelated(center)
            else:
                assert_centers_uuid_3_unrelated(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_bool]': 'false'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[attr_str]': 'one,two'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 2
        assert {obj['id'] for obj in data} == {UUID_1, UUID_2}
        for center in data:
            if center['id'] == UUID_1:
                assert_centers_uuid_1_related(center)
            else:  # center['id'] == UUID_2
                assert_centers_uuid_2_unrelated(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local]': UUID_11}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local]': f'{UUID_11},{UUID_12}'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote]': UUID_21}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote]': f'{UUID_21},{UUID_22}'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one]': UUID_31}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one]': f'{UUID_31},{UUID_32}'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys]': UUID_41}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys]': f'{UUID_41},{UUID_42}'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys]': UUID_51}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys]': f'{UUID_51},{UUID_52}'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    # strings containing a comma

    await model_extend(client)

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_attr_int]': '211'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_attr_int]': '211,212'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_attr_int]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_attr_str]': '11L-one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_attr_str]': '11L-one,11L-two'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_attr_str]': 'XXXXXXXX'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_one_one_local]': UUID_111}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_one_one_local]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_one_one_local]': f'{UUID_111},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_one_one_remote]': UUID_121}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_one_one_remote]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {
        'filter[one_one_local.ool_one_one_remote]': f'{UUID_121},88888888'
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_many_one]': UUID_131}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_many_one]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_many_one]': f'{UUID_131},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_one_manys]': UUID_141}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_one_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_one_manys]': f'{UUID_141},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_many_manys]': UUID_151}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_many_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_local.ool_many_manys]': f'{UUID_151},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_attr_int]': '121'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_attr_int]': '121,122'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_attr_int]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_attr_str]': '11R-one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_attr_str]': '11R-one,11R-two'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_attr_str]': 'XXXXXXXX'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_one_one_local]': UUID_211}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_one_one_local]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {
        'filter[one_one_remote.oor_one_one_local]': f'{UUID_211},88888888'
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_one_one_remote]': UUID_221}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_one_one_remote]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {
        'filter[one_one_remote.oor_one_one_remote]': f'{UUID_221},88888888'
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_many_one]': UUID_231}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_many_one]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_many_one]': f'{UUID_231},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_one_manys]': UUID_241}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_one_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_one_manys]': f'{UUID_241},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_many_manys]': UUID_251}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_many_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_one_remote.oor_many_manys]': f'{UUID_251},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_attr_int]': '811'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_attr_int]': '811,812'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_attr_int]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_attr_str]': 'M1-one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_attr_str]': 'M1-one,M1-two'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_attr_str]': 'XXXXXXXX'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_one_local]': UUID_311}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_one_local]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_one_local]': f'{UUID_311},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_one_remote]': UUID_321}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_one_remote]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_one_remote]': f'{UUID_321},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_many_one]': UUID_331}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_many_one]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_many_one]': f'{UUID_331},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_manys]': UUID_341}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_one_manys]': f'{UUID_341},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_many_manys]': UUID_351}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_many_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_one.mo_many_manys]': f'{UUID_351},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_attr_int]': '181,182'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_attr_int]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_attr_str]': '1M-one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_attr_str]': '1M-one,1M-two'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_attr_str]': 'XXXXXXXX'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_one_local]': UUID_411}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_one_local]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_one_local]': f'{UUID_411},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_one_remote]': UUID_421}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_one_remote]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_one_remote]': f'{UUID_421},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_many_one]': UUID_431}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_many_one]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_many_one]': f'{UUID_431},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_manys]': UUID_441}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_one_manys]': f'{UUID_441},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_many_manys]': UUID_451}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_many_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[one_manys.om_many_manys]': f'{UUID_451},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_attr_int]': '881,882'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_attr_int]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_attr_str]': 'MM-one'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_attr_str]': 'MM-one,MM-two'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_attr_str]': 'XXXXXXXX'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_one_local]': UUID_511}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_one_local]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_one_local]': f'{UUID_511},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_one_remote]': UUID_521}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_one_remote]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_one_remote]': f'{UUID_521},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_many_one]': UUID_531}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_many_one]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_many_one]': f'{UUID_531},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_manys]': UUID_541}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_one_manys]': f'{UUID_541},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_many_manys]': UUID_551}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_many_manys]': '88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document

    url = '/resources/centers'
    params = {'filter[many_manys.mm_many_manys]': f'{UUID_551},88888888'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        center = data[0]
        assert_centers_uuid_1_related(center)
        assert 'included' not in document


@pytest.mark.asyncio
async def test_get_collection_query_sort(client):
    """Functional test for a successful GET /resources/{collection}?sort
    request.
    """

    # pylint: disable=too-many-statements

    await model_init(client)

    url = '/resources/centers'
    params = {'sort': 'attr_int,-attr_str'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 3
        assert [obj['id'] for obj in data] == [UUID_1, UUID_2, UUID_3]
        for center in data:
            if center['id'] == UUID_1:
                assert_centers_uuid_1_related(center)
            elif center['id'] == UUID_2:
                assert_centers_uuid_2_unrelated(center)
            elif center['id'] == UUID_3:
                assert_centers_uuid_3_unrelated(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {'sort': 'attr_str,-attr_int'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 3
        assert [obj['id'] for obj in data] == [UUID_1, UUID_3, UUID_2]
        # one, three, two
        for center in data:
            if center['id'] == UUID_1:
                assert_centers_uuid_1_related(center)
            elif center['id'] == UUID_2:
                assert_centers_uuid_2_unrelated(center)
            elif center['id'] == UUID_3:
                assert_centers_uuid_3_unrelated(center)
        assert 'included' not in document


@pytest.mark.asyncio
async def test_get_collection_query_page(client):
    """Functional test for a successful /resources/{collection}?page[x]=x
    request.
    """

    url = '/resources/centers'
    params = {
        'page[number]': '0',
        'page[size]': '10',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []

    await model_init(client)

    url = '/resources/centers'
    params = {
        'page[number]': '0',
        'page[size]': '10',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 3
        assert {obj['id'] for obj in data} == {UUID_1, UUID_2, UUID_3}
        for center in data:
            if center['id'] == UUID_1:
                assert_centers_uuid_1_related(center)
            elif center['id'] == UUID_2:
                assert_centers_uuid_2_unrelated(center)
            else:  # center['id'] == UUID_3
                assert_centers_uuid_3_unrelated(center)
        assert 'included' not in document

    url = '/resources/centers'
    params = {
        'page[number]': '0',
        'page[size]': '1',
        'sort': 'attr_str',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        obj = data[0]
        assert obj['id'] == UUID_1
        for center in data:
            assert_centers_uuid_1_related(center)
        assert 'included' not in document


@pytest.mark.asyncio
async def test_get_collection_query_filter_include(client):
    """Functional test for a successful GET
    /resources/{collection}?filter[x]=x&include=x request.
    """

    await model_init(client)
    await model_extend(client)

    tests = [
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '1',
                'include': 'one_one_local',
            },
            'data_ids': {UUID_1},
            'included_ids': {UUID_11},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '1',
                'include': 'one_one_remote',
            },
            'data_ids': {UUID_1},
            'included_ids': {UUID_21},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '1',
                'include': 'many_one',
            },
            'data_ids': {UUID_1},
            'included_ids': {UUID_31},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '1',
                'include': 'one_manys',
            },
            'data_ids': {UUID_1},
            'included_ids': {UUID_41, UUID_42},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '1',
                'include': 'many_manys',
            },
            'data_ids': {UUID_1},
            'included_ids': {UUID_51, UUID_52},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '2',
                'include': 'one_one_local',
            },
            'data_ids': {UUID_2},
            'included_ids': {UUID_14},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '2',
                'include': 'one_one_remote',
            },
            'data_ids': {UUID_2},
            'included_ids': {UUID_24},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '2',
                'include': 'many_one',
            },
            'data_ids': {UUID_2},
            'included_ids': {UUID_34},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '2',
                'include': 'one_manys',
            },
            'data_ids': {UUID_2},
            'included_ids': {UUID_44, UUID_45},
        },
        {
            'url': '/resources/centers',
            'params': {
                'filter[attr_int]': '2',
                'include': 'many_manys',
            },
            'data_ids': {UUID_2},
            'included_ids': {UUID_54, UUID_55},
        },
        {
            'url': '/resources/ool_one_one_locals',
            'params': {
                'filter[one_one_local.center]': UUID_1,
                'include': 'one_one_local',
            },
            'data_ids': {UUID_111},
            'included_ids': {UUID_11},
        },
        {
            'url': '/resources/ool_one_one_remotes',
            'params': {
                'filter[one_one_local.center]': UUID_1,
                'include': 'one_one_local',
            },
            'data_ids': {UUID_121},
            'included_ids': {UUID_11},
        },
        {
            'url': '/resources/ool_many_ones',
            'params': {
                'filter[one_one_locals.center]': UUID_1,
                'include': 'one_one_locals',
            },
            'data_ids': {UUID_131},
            'included_ids': {UUID_11},
        },
        {
            'url': '/resources/ool_one_manys',
            'params': {
                'filter[one_one_local.center]': UUID_1,
                'include': 'one_one_local',
            },
            'data_ids': {UUID_141, UUID_142},
            'included_ids': {UUID_11},
        },
        {
            'url': '/resources/ool_many_manys',
            'params': {
                'filter[one_one_locals.center]': UUID_1,
                'include': 'one_one_locals',
            },
            'data_ids': {UUID_151, UUID_152},
            'included_ids': {UUID_11},
        },
        {
            'url': '/resources/oor_one_one_locals',
            'params': {
                'filter[one_one_remote.center]': UUID_1,
                'include': 'one_one_remote',
            },
            'data_ids': {UUID_211},
            'included_ids': {UUID_21},
        },
        {
            'url': '/resources/oor_one_one_remotes',
            'params': {
                'filter[one_one_remote.center]': UUID_1,
                'include': 'one_one_remote',
            },
            'data_ids': {UUID_221},
            'included_ids': {UUID_21},
        },
        {
            'url': '/resources/oor_many_ones',
            'params': {
                'filter[one_one_remotes.center]': UUID_1,
                'include': 'one_one_remotes',
            },
            'data_ids': {UUID_231},
            'included_ids': {UUID_21},
        },
        {
            'url': '/resources/oor_one_manys',
            'params': {
                'filter[one_one_remote.center]': UUID_1,
                'include': 'one_one_remote',
            },
            'data_ids': {UUID_241, UUID_242},
            'included_ids': {UUID_21},
        },
        {
            'url': '/resources/oor_many_manys',
            'params': {
                'filter[one_one_remotes.center]': UUID_1,
                'include': 'one_one_remotes',
            },
            'data_ids': {UUID_251, UUID_252},
            'included_ids': {UUID_21},
        },
        {
            'url': '/resources/mo_one_one_locals',
            'params': {
                'filter[many_one.centers]': UUID_1,
                'include': 'many_one',
            },
            'data_ids': {UUID_311},
            'included_ids': {UUID_31},
        },
        {
            'url': '/resources/mo_one_one_remotes',
            'params': {
                'filter[many_one.centers]': UUID_1,
                'include': 'many_one',
            },
            'data_ids': {UUID_321},
            'included_ids': {UUID_31},
        },
        {
            'url': '/resources/mo_many_ones',
            'params': {
                'filter[many_ones.centers]': UUID_1,
                'include': 'many_ones',
            },
            'data_ids': {UUID_331},
            'included_ids': {UUID_31},
        },
        {
            'url': '/resources/mo_one_manys',
            'params': {
                'filter[many_one.centers]': UUID_1,
                'include': 'many_one',
            },
            'data_ids': {UUID_341, UUID_342},
            'included_ids': {UUID_31},
        },
        {
            'url': '/resources/mo_many_manys',
            'params': {
                'filter[many_ones.centers]': UUID_1,
                'include': 'many_ones',
            },
            'data_ids': {UUID_351, UUID_352},
            'included_ids': {UUID_31},
        },
        {
            'url': '/resources/om_one_one_locals',
            'params': {
                'filter[one_many.center]': UUID_1,
                'include': 'one_many',
            },
            'data_ids': {UUID_411},
            'included_ids': {UUID_41},
        },
        {
            'url': '/resources/om_one_one_remotes',
            'params': {
                'filter[one_many.center]': UUID_1,
                'include': 'one_many',
            },
            'data_ids': {UUID_421},
            'included_ids': {UUID_41},
        },
        {
            'url': '/resources/om_many_ones',
            'params': {
                'filter[one_manys.center]': UUID_1,
                'include': 'one_manys',
            },
            'data_ids': {UUID_431},
            'included_ids': {UUID_41},
        },
        {
            'url': '/resources/om_one_manys',
            'params': {
                'filter[one_many.center]': UUID_1,
                'include': 'one_many',
            },
            'data_ids': {UUID_441, UUID_442},
            'included_ids': {UUID_41},
        },
        {
            'url': '/resources/om_many_manys',
            'params': {
                'filter[one_manys.center]': UUID_1,
                'include': 'one_manys',
            },
            'data_ids': {UUID_451, UUID_452},
            'included_ids': {UUID_41},
        },
        {
            'url': '/resources/mm_one_one_locals',
            'params': {
                'filter[many_many.centers]': UUID_1,
                'include': 'many_many',
            },
            'data_ids': {UUID_511},
            'included_ids': {UUID_51},
        },
        {
            'url': '/resources/mm_one_one_remotes',
            'params': {
                'filter[many_many.centers]': UUID_1,
                'include': 'many_many',
            },
            'data_ids': {UUID_521},
            'included_ids': {UUID_51},
        },
        {
            'url': '/resources/mm_many_ones',
            'params': {
                'filter[many_manys.centers]': UUID_1,
                'include': 'many_manys',
            },
            'data_ids': {UUID_531},
            'included_ids': {UUID_51},
        },
        {
            'url': '/resources/mm_one_manys',
            'params': {
                'filter[many_many.centers]': UUID_1,
                'include': 'many_many',
            },
            'data_ids': {UUID_541, UUID_542},
            'included_ids': {UUID_51},
        },
        {
            'url': '/resources/mm_many_manys',
            'params': {
                'filter[many_manys.centers]': UUID_1,
                'include': 'many_manys',
            },
            'data_ids': {UUID_551, UUID_552},
            'included_ids': {UUID_51},
        },
    ]

    for test in tests:
        async with client.get(test['url'],
                              params=test['params'],
                              headers=HEADERS) as response:
            document = await assert_get_collection(response)
            data = document['data']
            assert len(data) == len(
                test['data_ids']), 'unexpected len(data_ids)'
            assert {obj['id'] for obj in data
                   } == test['data_ids'], 'unexpected data_ids'
            included = document['included']
            assert len(included) == len(
                test['included_ids']), 'unexpected len(included_ids)'
            assert {obj['id'] for obj in included
                   } == test['included_ids'], 'unexpected included_ids'


@pytest.mark.asyncio
async def test_get_collection_query_include_page(client):
    """Functional test for a successful GET
    /resources/{collection}?include=x&page[x]=x request.
    """
    # pylint: disable=too-many-statements

    await model_init(client)
    await model_extend(client)

    url = '/resources/centers'
    params = {
        'include': 'one_one_local',
        'page[number]': '0',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_1}
        included = document['included']
        assert len(included) == 1
        assert {obj['id'] for obj in included} == {UUID_11}

    url = '/resources/centers'
    params = {
        'include': 'one_one_local',
        'page[number]': '1',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_2}
        included = document['included']
        assert len(included) == 1
        assert {obj['id'] for obj in included} == {UUID_14}

    url = '/resources/centers'
    params = {
        'include': 'one_one_remote',
        'page[number]': '0',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_1}
        included = document['included']
        assert len(included) == 1
        assert {obj['id'] for obj in included} == {UUID_21}

    url = '/resources/centers'
    params = {
        'include': 'one_one_remote',
        'page[number]': '1',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_2}
        included = document['included']
        assert len(included) == 1
        assert {obj['id'] for obj in included} == {UUID_24}

    url = '/resources/centers'
    params = {
        'include': 'many_one',
        'page[number]': '0',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_1}
        included = document['included']
        assert len(included) == 1
        assert {obj['id'] for obj in included} == {UUID_31}

    url = '/resources/centers'
    params = {
        'include': 'many_one',
        'page[number]': '1',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_2}
        included = document['included']
        assert len(included) == 1
        assert {obj['id'] for obj in included} == {UUID_34}

    url = '/resources/centers'
    params = {
        'include': 'one_manys',
        'page[number]': '0',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_1}
        included = document['included']
        assert len(included) == 2
        assert {obj['id'] for obj in included} == {UUID_41, UUID_42}

    url = '/resources/centers'
    params = {
        'include': 'one_manys',
        'page[number]': '1',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_2}
        included = document['included']
        assert len(included) == 2
        assert {obj['id'] for obj in included} == {UUID_44, UUID_45}

    url = '/resources/centers'
    params = {
        'include': 'many_manys',
        'page[number]': '0',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_1}
        included = document['included']
        assert len(included) == 2
        assert {obj['id'] for obj in included} == {UUID_51, UUID_52}

    url = '/resources/centers'
    params = {
        'include': 'many_manys',
        'page[number]': '1',
        'page[size]': '1',
        'sort': 'attr_int',
    }
    async with client.get(url, params=params, headers=HEADERS) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert len(data) == 1
        assert {obj['id'] for obj in data} == {UUID_2}
        included = document['included']
        assert len(included) == 2
        assert {obj['id'] for obj in included} == {UUID_54, UUID_55}


@pytest.mark.asyncio
async def test_get_collection_accept_no_parameter(client):
    """Functional tests for a GET /resources/{collection} request with an Accept
    header where some (but not all) instances of the JSON API media type
    ('application/vnd.api+json') are modified with media type parameters.
    """

    url = '/resources/centers'
    headers = {
        'Accept':
            'application/vnd.api+json;xxxxxxxx=0,application/vnd.api+json',
    }
    async with client.get(url, headers=headers) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document


@pytest.mark.asyncio
async def test_get_collection_no_accept(client):
    """Functional tests for a GET /resources/{collection} request without an
    Accept header.
    """

    url = '/resources/centers'
    headers = {}
    async with client.get(url, headers=headers) as response:
        document = await assert_get_collection(response)
        data = document['data']
        assert data == []
        assert 'included' not in document


#
# Failed requests
#
@pytest.mark.asyncio
async def test_get_collection_content_type_parameter(client):
    """Functional tests for a failed GET /resources/{collection} request where
    the Content-Type header contains a parameter.
    """

    url = '/resources/centers'
    headers = {
        'Content-Type': 'application/vnd.api+json;xxxxxxxx=0',
    }
    async with client.get(url, headers=headers) as response:
        await assert_content_type_parameter(response)


@pytest.mark.asyncio
async def test_get_collection_accept_parameters(client):
    """Functional tests for a failed GET /resources/{collection} request with
    the Accept header where all instances of the JSON API media type
    ('application/vnd.api+json') are modified with media type parameters.
    """

    url = '/resources/centers'
    headers = {
        'Accept': 'application/vnd.api+json;xxxxxxxx=0',
    }
    async with client.get(url, headers=headers) as response:
        await assert_accept_parameters(response)


@pytest.mark.asyncio
async def test_get_collection_duplicate_include(client):
    """Functional test for a failed GET /resources/{collection}?include=x
    request due to too repeated 'include' parameters.
    """

    url = '/resources/centers'
    params = MultiDict([('include', 'one_one_locall'),
                        ('include', 'one_one_remote')])
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_duplicate_parameters(response, 'include')


@pytest.mark.asyncio
async def test_get_collection_include_invalid_path(client):
    """Functional test for a failed GET /resources/{collection}?include=x
    request due to an invalid relationship path.
    """

    await model_init(client)
    await model_extend(client)

    url = '/resources/centers'
    params = {'include': 'xxxxxxxx'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_include_invalid_path(response, 'xxxxxxxx')
    url = '/resources/centers'
    params = {'include': 'one_one_local.xxxxxxxx'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_include_invalid_path(response, 'one_one_local.xxxxxxxx')
    url = '/resources/centers'
    params = {'include': 'one_one_local.ool_one_one_local.xxxxxxxx'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_include_invalid_path(
            response, 'one_one_local.ool_one_one_local.xxxxxxxx')


@pytest.mark.asyncio
async def test_get_collection_fields_invalid_resource(client):
    """Functional test for a failed GET /resources/{collection}?fields[x]=x
    request where the resource does not exist.
    """

    await model_init(client)
    await model_extend(client)

    url = '/resources/centers'
    params = {'fields[xxxxxxxx]': 'attr_int'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_fields_invalid_resource(response, 'xxxxxxxx')


@pytest.mark.asyncio
async def test_get_collection_fields_invalid_field(client):
    """Functional test for a failed GET /resources/{collection}?fields[x]=x
    request where the field does not exist.
    """

    await model_init(client)
    await model_extend(client)

    url = '/resources/centers'
    params = {'fields[centers]': 'xxxxxxxx'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_fields_invalid_field(response, 'centers', 'xxxxxxxx')

    url = '/resources/centers'
    params = {'fields[centers]': 'type'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_fields_invalid_field(response, 'centers', 'type')

    url = '/resources/centers'
    params = {'fields[centers]': 'id'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_fields_invalid_field(response, 'centers', 'id')


@pytest.mark.asyncio
async def test_get_collection_filter_invalid_path(client):
    """Functional test for a failed GET /resources/{collection}?filter[x]=x
    request where the field does not exist.
    """

    await model_init(client)
    await model_extend(client)

    url = '/resources/centers'
    params = {'filter[xxxxxxxx]': '0'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_filter_invalid_path(response, 'xxxxxxxx')

    url = '/resources/centers'
    params = {'filter[xxxxxxxx.x]': '0'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_filter_invalid_path(response, 'xxxxxxxx')

    url = '/resources/centers'
    params = {'filter[one_manys.xxxxxxxx.x]': '0'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_filter_invalid_path(response, 'one_manys.xxxxxxxx')

    url = '/resources/centers'
    params = {'filter[many_manys.xxxxxxxx.x]': '0'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_filter_invalid_path(response, 'many_manys.xxxxxxxx')


@pytest.mark.asyncio
async def test_get_collection_sort_invalid_field(client):
    """Functional test for a failed GET /resources/{collection}?sort=x request
    where the field does not exist.
    """

    await model_init(client)
    await model_extend(client)

    url = '/resources/centers'
    params = {'sort': 'xxxxxxxx'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_sort_invalid_field(response, 'xxxxxxxx')

    url = '/resources/centers'
    params = {'sort': '-xxxxxxxx'}
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_sort_invalid_field(response, 'xxxxxxxx')


@pytest.mark.asyncio
async def test_get_collection_duplicate_sort(client):
    """Functional test for a failed GET /resources/{collection}?sort=x request
    due to too repeated 'sort' parameters.
    """

    url = '/resources/centers'
    params = MultiDict([('sort', 'attr_int'), ('sort', 'attr_str')])
    async with client.get(url, params=params, headers=HEADERS) as response:
        await assert_duplicate_parameters(response, 'sort')


@pytest.mark.asyncio
async def test_get_collection_nonexistent_collection(client):
    """Functional tests for a failed GET /resources/{collection} reqeust where
    {collection} does not exist.
    """

    url = '/resources/xxxxxxxxs'
    async with client.get(url, headers=HEADERS) as response:
        await assert_get_collection_nonexistent_collection(response, url)
