# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Common post operations for functional tests."""

# pylint: disable=too-many-lines

from ajsonapi.functests.asserts.post_collection import (
    assert_post_collection_with_id,
    assert_post_collection_without_id,
)
from ajsonapi.functests.headers import SEND_HEADERS
from ajsonapi.functests.model_objects import (
    JSON_CENTERS,
    JSON_CENTERS_UUID_1,
    JSON_CENTERS_UUID_2,
    JSON_CENTERS_UUID_3,
    JSON_MANY_MANYS_UUID_51,
    JSON_MANY_MANYS_UUID_52,
    JSON_MANY_MANYS_UUID_53,
    JSON_MANY_MANYS_UUID_54,
    JSON_MANY_MANYS_UUID_55,
    JSON_MANY_ONES_UUID_31,
    JSON_MANY_ONES_UUID_32,
    JSON_MANY_ONES_UUID_34,
    JSON_MM_MANY_MANYS_UUID_551,
    JSON_MM_MANY_MANYS_UUID_552,
    JSON_MM_MANY_ONES_UUID_531,
    JSON_MM_ONE_MANYS_UUID_541,
    JSON_MM_ONE_MANYS_UUID_542,
    JSON_MM_ONE_ONE_LOCALS_UUID_511,
    JSON_MM_ONE_ONE_REMOTES_UUID_521,
    JSON_MO_MANY_MANYS_UUID_351,
    JSON_MO_MANY_MANYS_UUID_352,
    JSON_MO_MANY_ONES_UUID_331,
    JSON_MO_ONE_MANYS_UUID_341,
    JSON_MO_ONE_MANYS_UUID_342,
    JSON_MO_ONE_ONE_LOCALS_UUID_311,
    JSON_MO_ONE_ONE_REMOTES_UUID_321,
    JSON_OM_MANY_MANYS_UUID_451,
    JSON_OM_MANY_MANYS_UUID_452,
    JSON_OM_MANY_ONES_UUID_431,
    JSON_OM_ONE_MANYS_UUID_441,
    JSON_OM_ONE_MANYS_UUID_442,
    JSON_OM_ONE_ONE_LOCALS_UUID_411,
    JSON_OM_ONE_ONE_REMOTES_UUID_421,
    JSON_ONE_MANYS_UUID_41,
    JSON_ONE_MANYS_UUID_42,
    JSON_ONE_MANYS_UUID_43,
    JSON_ONE_MANYS_UUID_44,
    JSON_ONE_MANYS_UUID_45,
    JSON_ONE_ONE_LOCALS_UUID_11,
    JSON_ONE_ONE_LOCALS_UUID_12,
    JSON_ONE_ONE_LOCALS_UUID_14,
    JSON_ONE_ONE_REMOTES_UUID_21,
    JSON_ONE_ONE_REMOTES_UUID_22,
    JSON_ONE_ONE_REMOTES_UUID_24,
    JSON_OOL_MANY_MANYS_UUID_151,
    JSON_OOL_MANY_MANYS_UUID_152,
    JSON_OOL_MANY_MANYS_UUID_159,
    JSON_OOL_MANY_ONES_UUID_131,
    JSON_OOL_MANY_ONES_UUID_139,
    JSON_OOL_ONE_MANYS_UUID_141,
    JSON_OOL_ONE_MANYS_UUID_142,
    JSON_OOL_ONE_MANYS_UUID_149,
    JSON_OOL_ONE_ONE_LOCALS_UUID_111,
    JSON_OOL_ONE_ONE_LOCALS_UUID_119,
    JSON_OOL_ONE_ONE_REMOTES_UUID_121,
    JSON_OOL_ONE_ONE_REMOTES_UUID_129,
    JSON_OOR_MANY_MANYS_UUID_251,
    JSON_OOR_MANY_MANYS_UUID_252,
    JSON_OOR_MANY_ONES_UUID_231,
    JSON_OOR_ONE_MANYS_UUID_241,
    JSON_OOR_ONE_MANYS_UUID_242,
    JSON_OOR_ONE_ONE_LOCALS_UUID_211,
    JSON_OOR_ONE_ONE_REMOTES_UUID_221,
    UUID_1,
    UUID_2,
    UUID_3,
    UUID_11,
    UUID_12,
    UUID_14,
    UUID_21,
    UUID_22,
    UUID_24,
    UUID_31,
    UUID_32,
    UUID_34,
    UUID_41,
    UUID_42,
    UUID_43,
    UUID_44,
    UUID_45,
    UUID_51,
    UUID_52,
    UUID_53,
    UUID_54,
    UUID_55,
    UUID_111,
    UUID_119,
    UUID_121,
    UUID_129,
    UUID_131,
    UUID_139,
    UUID_141,
    UUID_142,
    UUID_149,
    UUID_151,
    UUID_152,
    UUID_159,
    UUID_211,
    UUID_221,
    UUID_231,
    UUID_241,
    UUID_242,
    UUID_251,
    UUID_252,
    UUID_311,
    UUID_321,
    UUID_331,
    UUID_341,
    UUID_342,
    UUID_351,
    UUID_352,
    UUID_411,
    UUID_421,
    UUID_431,
    UUID_441,
    UUID_442,
    UUID_451,
    UUID_452,
    UUID_511,
    UUID_521,
    UUID_531,
    UUID_541,
    UUID_542,
    UUID_551,
    UUID_552,
)


async def post_centers_uuid_1(client):
    """Successful POST /centers with id=UUID_1."""

    url = '/resources/centers'
    json = JSON_CENTERS_UUID_1

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'centers'
        assert event['data']['id'] == UUID_1


async def post_centers_uuid_2(client):
    """Successful POST /centers with id=UUID_2."""

    url = '/resources/centers'
    json = JSON_CENTERS_UUID_2

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'centers'
        assert event['data']['id'] == UUID_2


async def post_centers_uuid_3(client):
    """Successful POST /centers with id=UUID_3."""

    url = '/resources/centers'
    json = JSON_CENTERS_UUID_3

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'centers'
        assert event['data']['id'] == UUID_3


async def post_centers(client):
    """Successful POST /centers without id."""

    url = '/resources/centers'
    json = JSON_CENTERS

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            document = await assert_post_collection_without_id(response)
            center_id = document['data']['id']
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'centers'
        assert event['data']['id'] == center_id
    return document


async def post_one_one_locals_uuid_11(client):
    """Successful POST /one_one_locals with id=UUID_11."""
    url = '/resources/one_one_locals'
    json = JSON_ONE_ONE_LOCALS_UUID_11

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_one_locals'
        assert event['data']['id'] == UUID_11


async def post_one_one_locals_uuid_12(client):
    """Successful POST /one_one_locals with id=UUID_12."""
    url = '/resources/one_one_locals'
    json = JSON_ONE_ONE_LOCALS_UUID_12

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_one_locals'
        assert event['data']['id'] == UUID_12


async def post_one_one_locals_uuid_14(client):
    """Successful POST /one_one_locals with id=UUID_14."""
    url = '/resources/one_one_locals'
    json = JSON_ONE_ONE_LOCALS_UUID_14

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_one_locals'
        assert event['data']['id'] == UUID_14


async def post_one_one_remotes_uuid_21(client):
    """Successful POST /one_one_remotes with id=UUID_21."""
    url = '/resources/one_one_remotes'
    json = JSON_ONE_ONE_REMOTES_UUID_21

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_one_remotes'
        assert event['data']['id'] == UUID_21


async def post_one_one_remotes_uuid_22(client):
    """Successful POST /one_one_remotes with id=UUID_22."""
    url = '/resources/one_one_remotes'
    json = JSON_ONE_ONE_REMOTES_UUID_22

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_one_remotes'
        assert event['data']['id'] == UUID_22


async def post_one_one_remotes_uuid_24(client):
    """Successful POST /one_one_remotes with id=UUID_24."""
    url = '/resources/one_one_remotes'
    json = JSON_ONE_ONE_REMOTES_UUID_24

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_one_remotes'
        assert event['data']['id'] == UUID_24


async def post_many_ones_uuid_31(client):
    """Successful POST /many_ones with id=UUID_31."""
    url = '/resources/many_ones'
    json = JSON_MANY_ONES_UUID_31

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'many_ones'
        assert event['data']['id'] == UUID_31


async def post_many_ones_uuid_32(client):
    """Successful POST /many_ones with id=UUID_32."""
    url = '/resources/many_ones'
    json = JSON_MANY_ONES_UUID_32

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'many_ones'
        assert event['data']['id'] == UUID_32


async def post_many_ones_uuid_34(client):
    """Successful POST /many_ones with id=UUID_34."""
    url = '/resources/many_ones'
    json = JSON_MANY_ONES_UUID_34

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'many_ones'
        assert event['data']['id'] == UUID_34


async def post_one_manys_uuid_41(client):
    """Successful POST /one_manys with id=UUID_41."""
    url = '/resources/one_manys'
    json = JSON_ONE_MANYS_UUID_41

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_manys'
        assert event['data']['id'] == UUID_41


async def post_one_manys_uuid_42(client):
    """Successful POST /one_manys with id=UUID_42."""
    url = '/resources/one_manys'
    json = JSON_ONE_MANYS_UUID_42

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_manys'
        assert event['data']['id'] == UUID_42


async def post_one_manys_uuid_43(client):
    """Successful POST /one_manys with id=UUID_43."""
    url = '/resources/one_manys'
    json = JSON_ONE_MANYS_UUID_43

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_manys'
        assert event['data']['id'] == UUID_43


async def post_one_manys_uuid_44(client):
    """Successful POST /one_manys with id=UUID_44."""
    url = '/resources/one_manys'
    json = JSON_ONE_MANYS_UUID_44

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_manys'
        assert event['data']['id'] == UUID_44


async def post_one_manys_uuid_45(client):
    """Successful POST /one_manys with id=UUID_45."""
    url = '/resources/one_manys'
    json = JSON_ONE_MANYS_UUID_45

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'one_manys'
        assert event['data']['id'] == UUID_45


async def post_many_manys_uuid_51(client):
    """Successful POST /many_manys with id=UUID_51."""
    url = '/resources/many_manys'
    json = JSON_MANY_MANYS_UUID_51

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'many_manys'
        assert event['data']['id'] == UUID_51


async def post_many_manys_uuid_52(client):
    """Successful POST /many_manys with id=UUID_52."""
    url = '/resources/many_manys'
    json = JSON_MANY_MANYS_UUID_52

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'many_manys'
        assert event['data']['id'] == UUID_52


async def post_many_manys_uuid_53(client):
    """Successful POST /many_manys with id=UUID_53."""
    url = '/resources/many_manys'
    json = JSON_MANY_MANYS_UUID_53

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'many_manys'
        assert event['data']['id'] == UUID_53


async def post_many_manys_uuid_54(client):
    """Successful POST /many_manys with id=UUID_54."""
    url = '/resources/many_manys'
    json = JSON_MANY_MANYS_UUID_54

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'many_manys'
        assert event['data']['id'] == UUID_54


async def post_many_manys_uuid_55(client):
    """Successful POST /many_manys with id=UUID_55."""
    url = '/resources/many_manys'
    json = JSON_MANY_MANYS_UUID_55

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'many_manys'
        assert event['data']['id'] == UUID_55


async def post_ool_one_one_locals_uuid_111(client):
    """Successful POST /ool_one_one_locals with id=UUID_111."""
    url = '/resources/ool_one_one_locals'
    json = JSON_OOL_ONE_ONE_LOCALS_UUID_111

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_one_one_locals'
        assert event['data']['id'] == UUID_111


async def post_ool_one_one_locals_uuid_119(client):
    """Successful POST /ool_one_one_locals with id=UUID_119."""
    url = '/resources/ool_one_one_locals'
    json = JSON_OOL_ONE_ONE_LOCALS_UUID_119

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_one_one_locals'
        assert event['data']['id'] == UUID_119


async def post_ool_one_one_remotes_uuid_121(client):
    """Successful POST /ool_one_one_remotes with id=UUID_121."""
    url = '/resources/ool_one_one_remotes'
    json = JSON_OOL_ONE_ONE_REMOTES_UUID_121

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_one_one_remotes'
        assert event['data']['id'] == UUID_121


async def post_ool_one_one_remotes_uuid_129(client):
    """Successful POST /ool_one_one_remotes with id=UUID_129."""
    url = '/resources/ool_one_one_remotes'
    json = JSON_OOL_ONE_ONE_REMOTES_UUID_129

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_one_one_remotes'
        assert event['data']['id'] == UUID_129


async def post_ool_many_ones_uuid_131(client):
    """Successful POST /ool_many_ones with id=UUID_131."""
    url = '/resources/ool_many_ones'
    json = JSON_OOL_MANY_ONES_UUID_131

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_many_ones'
        assert event['data']['id'] == UUID_131


async def post_ool_many_ones_uuid_139(client):
    """Successful POST /ool_many_ones with id=UUID_139."""
    url = '/resources/ool_many_ones'
    json = JSON_OOL_MANY_ONES_UUID_139

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_many_ones'
        assert event['data']['id'] == UUID_139


async def post_ool_one_manys_uuid_141(client):
    """Successful POST /ool_one_manys with id=UUID_141."""
    url = '/resources/ool_one_manys'
    json = JSON_OOL_ONE_MANYS_UUID_141

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_one_manys'
        assert event['data']['id'] == UUID_141


async def post_ool_one_manys_uuid_142(client):
    """Successful POST /ool_one_manys with id=UUID_142."""
    url = '/resources/ool_one_manys'
    json = JSON_OOL_ONE_MANYS_UUID_142

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_one_manys'
        assert event['data']['id'] == UUID_142


async def post_ool_one_manys_uuid_149(client):
    """Successful POST /ool_one_manys with id=UUID_149."""
    url = '/resources/ool_one_manys'
    json = JSON_OOL_ONE_MANYS_UUID_149

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_one_manys'
        assert event['data']['id'] == UUID_149


async def post_ool_many_manys_uuid_151(client):
    """Successful POST /ool_many_manys with id=UUID_151."""
    url = '/resources/ool_many_manys'
    json = JSON_OOL_MANY_MANYS_UUID_151

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_many_manys'
        assert event['data']['id'] == UUID_151


async def post_ool_many_manys_uuid_152(client):
    """Successful POST /ool_many_manys with id=UUID_152."""
    url = '/resources/ool_many_manys'
    json = JSON_OOL_MANY_MANYS_UUID_152

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_many_manys'
        assert event['data']['id'] == UUID_152


async def post_ool_many_manys_uuid_159(client):
    """Successful POST /ool_many_manys with id=UUID_159."""
    url = '/resources/ool_many_manys'
    json = JSON_OOL_MANY_MANYS_UUID_159

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'ool_many_manys'
        assert event['data']['id'] == UUID_159


async def post_oor_one_one_locals_uuid_211(client):
    """Successful POST /oor_one_one_locals with id=UUID_211."""
    url = '/resources/oor_one_one_locals'
    json = JSON_OOR_ONE_ONE_LOCALS_UUID_211

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'oor_one_one_locals'
        assert event['data']['id'] == UUID_211


async def post_oor_one_one_remotes_uuid_221(client):
    """Successful POST /oor_one_one_remotes with id=UUID_221."""
    url = '/resources/oor_one_one_remotes'
    json = JSON_OOR_ONE_ONE_REMOTES_UUID_221

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'oor_one_one_remotes'
        assert event['data']['id'] == UUID_221


async def post_oor_many_ones_uuid_231(client):
    """Successful POST /oor_many_ones with id=UUID_231."""
    url = '/resources/oor_many_ones'
    json = JSON_OOR_MANY_ONES_UUID_231

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'oor_many_ones'
        assert event['data']['id'] == UUID_231


async def post_oor_one_manys_uuid_241(client):
    """Successful POST /oor_one_manys with id=UUID_241."""
    url = '/resources/oor_one_manys'
    json = JSON_OOR_ONE_MANYS_UUID_241

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'oor_one_manys'
        assert event['data']['id'] == UUID_241


async def post_oor_one_manys_uuid_242(client):
    """Successful POST /oor_one_manys with id=UUID_242."""
    url = '/resources/oor_one_manys'
    json = JSON_OOR_ONE_MANYS_UUID_242

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'oor_one_manys'
        assert event['data']['id'] == UUID_242


async def post_oor_many_manys_uuid_251(client):
    """Successful POST /oor_many_manys with id=UUID_251."""
    url = '/resources/oor_many_manys'
    json = JSON_OOR_MANY_MANYS_UUID_251

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'oor_many_manys'
        assert event['data']['id'] == UUID_251


async def post_oor_many_manys_uuid_252(client):
    """Successful POST /oor_many_manys with id=UUID_252."""
    url = '/resources/oor_many_manys'
    json = JSON_OOR_MANY_MANYS_UUID_252

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'oor_many_manys'
        assert event['data']['id'] == UUID_252


async def post_mo_one_one_locals_uuid_311(client):
    """Successful POST /mo_one_one_locals with id=UUID_311."""
    url = '/resources/mo_one_one_locals'
    json = JSON_MO_ONE_ONE_LOCALS_UUID_311

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mo_one_one_locals'
        assert event['data']['id'] == UUID_311


async def post_mo_one_one_remotes_uuid_321(client):
    """Successful POST /mo_one_one_remotes with id=UUID_321."""
    url = '/resources/mo_one_one_remotes'
    json = JSON_MO_ONE_ONE_REMOTES_UUID_321

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mo_one_one_remotes'
        assert event['data']['id'] == UUID_321


async def post_mo_many_ones_uuid_331(client):
    """Successful POST /mo_many_ones with id=UUID_331."""
    url = '/resources/mo_many_ones'
    json = JSON_MO_MANY_ONES_UUID_331

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mo_many_ones'
        assert event['data']['id'] == UUID_331


async def post_mo_one_manys_uuid_341(client):
    """Successful POST /mo_one_manys with id=UUID_341."""
    url = '/resources/mo_one_manys'
    json = JSON_MO_ONE_MANYS_UUID_341

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mo_one_manys'
        assert event['data']['id'] == UUID_341


async def post_mo_one_manys_uuid_342(client):
    """Successful POST /mo_one_manys with id=UUID_342."""
    url = '/resources/mo_one_manys'
    json = JSON_MO_ONE_MANYS_UUID_342

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mo_one_manys'
        assert event['data']['id'] == UUID_342


async def post_mo_many_manys_uuid_351(client):
    """Successful POST /mo_many_manys with id=UUID_351."""
    url = '/resources/mo_many_manys'
    json = JSON_MO_MANY_MANYS_UUID_351

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mo_many_manys'
        assert event['data']['id'] == UUID_351


async def post_mo_many_manys_uuid_352(client):
    """Successful POST /mo_many_manys with id=UUID_352."""
    url = '/resources/mo_many_manys'
    json = JSON_MO_MANY_MANYS_UUID_352

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mo_many_manys'
        assert event['data']['id'] == UUID_352


async def post_om_one_one_locals_uuid_411(client):
    """Successful POST /om_one_one_locals with id=UUID_411."""
    url = '/resources/om_one_one_locals'
    json = JSON_OM_ONE_ONE_LOCALS_UUID_411

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'om_one_one_locals'
        assert event['data']['id'] == UUID_411


async def post_om_one_one_remotes_uuid_421(client):
    """Successful POST /om_one_one_remotes with id=UUID_421."""
    url = '/resources/om_one_one_remotes'
    json = JSON_OM_ONE_ONE_REMOTES_UUID_421

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'om_one_one_remotes'
        assert event['data']['id'] == UUID_421


async def post_om_many_ones_uuid_431(client):
    """Successful POST /om_many_ones with id=UUID_431."""
    url = '/resources/om_many_ones'
    json = JSON_OM_MANY_ONES_UUID_431

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'om_many_ones'
        assert event['data']['id'] == UUID_431


async def post_om_one_manys_uuid_441(client):
    """Successful POST /om_one_manys with id=UUID_441."""
    url = '/resources/om_one_manys'
    json = JSON_OM_ONE_MANYS_UUID_441

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'om_one_manys'
        assert event['data']['id'] == UUID_441


async def post_om_one_manys_uuid_442(client):
    """Successful POST /om_one_manys with id=UUID_442."""
    url = '/resources/om_one_manys'
    json = JSON_OM_ONE_MANYS_UUID_442

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'om_one_manys'
        assert event['data']['id'] == UUID_442


async def post_om_many_manys_uuid_451(client):
    """Successful POST /om_many_manys with id=UUID_451."""
    url = '/resources/om_many_manys'
    json = JSON_OM_MANY_MANYS_UUID_451

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'om_many_manys'
        assert event['data']['id'] == UUID_451


async def post_om_many_manys_uuid_452(client):
    """Successful POST /om_many_manys with id=UUID_452."""
    url = '/resources/om_many_manys'
    json = JSON_OM_MANY_MANYS_UUID_452

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'om_many_manys'
        assert event['data']['id'] == UUID_452


async def post_mm_one_one_locals_uuid_511(client):
    """Successful POST /mm_one_one_locals with id=UUID_511."""
    url = '/resources/mm_one_one_locals'
    json = JSON_MM_ONE_ONE_LOCALS_UUID_511

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mm_one_one_locals'
        assert event['data']['id'] == UUID_511


async def post_mm_one_one_remotes_uuid_521(client):
    """Successful POST /mm_one_one_remotes with id=UUID_521."""
    url = '/resources/mm_one_one_remotes'
    json = JSON_MM_ONE_ONE_REMOTES_UUID_521

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mm_one_one_remotes'
        assert event['data']['id'] == UUID_521


async def post_mm_many_ones_uuid_531(client):
    """Successful POST /mm_many_ones with id=UUID_531."""
    url = '/resources/mm_many_ones'
    json = JSON_MM_MANY_ONES_UUID_531

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mm_many_ones'
        assert event['data']['id'] == UUID_531


async def post_mm_one_manys_uuid_541(client):
    """Successful POST /mm_one_manys with id=UUID_541."""
    url = '/resources/mm_one_manys'
    json = JSON_MM_ONE_MANYS_UUID_541

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mm_one_manys'
        assert event['data']['id'] == UUID_541


async def post_mm_one_manys_uuid_542(client):
    """Successful POST /mm_one_manys with id=UUID_542."""
    url = '/resources/mm_one_manys'
    json = JSON_MM_ONE_MANYS_UUID_542

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mm_one_manys'
        assert event['data']['id'] == UUID_542


async def post_mm_many_manys_uuid_551(client):
    """Successful POST /mm_many_manys with id=UUID_551."""
    url = '/resources/mm_many_manys'
    json = JSON_MM_MANY_MANYS_UUID_551

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mm_many_manys'
        assert event['data']['id'] == UUID_551


async def post_mm_many_manys_uuid_552(client):
    """Successful POST /mm_many_manys with id=UUID_552."""
    url = '/resources/mm_many_manys'
    json = JSON_MM_MANY_MANYS_UUID_552

    # pylint: disable=invalid-name
    async with client.ws_connect('/events') as ws:
        async with client.post(url, headers=SEND_HEADERS,
                               json=json) as response:
            await assert_post_collection_with_id(response)
        msg = await ws.receive()
        event = msg.json()
        assert event['op'] == 'create'
        assert event['data']['type'] == 'mm_many_manys'
        assert event['data']['id'] == UUID_552
