# Copyright © 2018-2020 Roel van der Goot
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
"""Verify that the targets of the Makefile work as expected."""

import re
import subprocess

MATCH_MAKE_STDERR = re.compile(r'make(\[\d\])?: \*\*\* \[\w*\] Error (\d*)')


def run_make_target(target, dry_run=False):
    """Helper function to run "make <target>"

    Args:
        target (str): target to make
        dry_run (:obj:`bool`, optional): Whether to use --dry-run flag when
            running "make". Defaults to False.

    Returns:
        int: "make" exit code.
        (int, int): "make" exit code plus "make target process" exit code.
    """
    if dry_run:
        command_list = ['make', '--dry-run', target]
    else:
        command_list = ['make', target]
    process = subprocess.run(command_list, stderr=subprocess.PIPE, check=True)
    if process.returncode:
        last_line = process.stderr.decode('utf-8').strip().split('\n')[-1]
        match = MATCH_MAKE_STDERR.match(last_line)
        if match:
            return process.returncode, int(match.group(2))
    return process.returncode


def test_make_lint():
    """Test "make lint"."""
    returncode = run_make_target('lint')
    assert returncode == 0


def test_make_tags():
    """Test "make tags"."""
    returncode = run_make_target('tags')
    assert returncode == 0


def test_make_func():
    """Test "make func"."""
    returncode = run_make_target('func')
    assert returncode == 0


def test_make_unit():
    """Test "make unit"."""
    returncode = run_make_target('unit')
    assert returncode == 0


def test_make_make():
    """Test "make make"."""
    returncode = run_make_target('make', dry_run=True)
    assert returncode == 0


def test_make_cov():
    """Test "make cov"."""
    returncode = run_make_target('cov')
    assert returncode == 0


def test_make_isort():
    """Test "make isort"."""
    returncode = run_make_target('isort')
    assert returncode == 0


def test_make_format():
    """Test "make format"."""
    returncode = run_make_target('format')
    assert returncode == 0


def test_make_yapf():
    """Test "make yapf"."""
    returncode = run_make_target('yapf')
    assert returncode == 0


def test_make_style():
    """Test "make style"."""
    returncode = run_make_target('style')
    assert returncode == 0


def test_make_freeze():
    """Test "make freeze"."""
    returncode = run_make_target('freeze')
    assert returncode == 0


def test_make_release():
    """Test "make release"."""
    returncode = run_make_target('release')
    assert returncode == 0


def test_make_public():
    """Test "make public"."""
    returncode = run_make_target('public', dry_run=True)
    assert returncode == 0


def test_make_clean():
    """Test "make clean"."""
    returncode = run_make_target('clean')
    assert returncode == 0


def test_make_unknown_target():
    """Test "make unknown_target"."""
    returncode = run_make_target('unknown_target')
    assert returncode == 2
