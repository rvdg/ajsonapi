# This works for Linux.
NUM_CORES = $(shell nproc)

FUNCTEST_DIRS := $(shell find . -type d -name functests)
MAKETEST_DIRS := $(shell find . -type d -name maketests)
PYCACHE_DIRS := $(shell find . -type d -name __pycache__)
UNITTEST_DIRS := $(shell find . -type d -name unittests)
PYTHON_FILES := $(shell find . -type f -name '*.py')


# Development targets

default: format lint tags cov


lint:
	pylint -j $(NUM_CORES) --disable=R0801 $(PYTHON_FILES)

tags:
	ctags -R --exclude=".git" .

# Testing targets

test:
	pytest -n $(NUM_CORES) $(FUNCTEST_DIRS) $(UNITTEST_DIRS)

func:
	pytest -n $(NUM_CORES) $(FUNCTEST_DIRS)

unit:
	pytest -n $(NUM_CORES) $(UNITTEST_DIRS)

make:
	pytest $(MAKETEST_DIRS)

cov:
	pytest -n $(NUM_CORES) --cov=. --cov-report=html --cov-report=term $(FUNCTEST_DIRS) $(UNITTEST_DIRS)

gitlab:
	pytest -n $(NUM_CORES) --cov=. --cov-report=term ajsonapi/functests/test_get_collection.py::test_get_collection

# Coding style targets

format: yapf isort


isort:
	isort $(PYTHON_FILES)

yapf:
	yapf -p -i --style=google $(PYTHON_FILES)

style:
	pycodestyle $(PYTHON_FILES)

# Build and release targets

freeze:
	pip freeze > requirements/frozen.txt

release:
	python3 setup.py sdist bdist_wheel

public:
	twine upload dist/*

# Clean up target

clean:
	rm -rf $(PYCACHE_DIRS) .coverage* .pytest_cache/ ajsonapi.egg-info/ build/ dist/ htmlcov/ tags
	sudo rm -rf /tmp/pgdata/ajsonapi*/


.PHONY: default lint tags test func unit make cov format isort yapf style freeze release public gitlab
